#include <iostream>
#include <QApplication>
#include "qt/output/main_window.hpp"

using namespace std;

int main(int argc,char** argv){
  QApplication app(argc,argv);
  QtMainWindow* window;
  window=new QtMainWindow;
  window->resize(1600,1000);
  window->show();
  return app.exec();
}
