#ifndef INPUT_DATA_HPP
#define INPUT_DATA_HPP

#include "physics.hpp"
#include "time.hpp"
#include "geometry.hpp"
#include "initial_state.hpp"
#include "source.hpp"


class InputData{
public:
  Geometry geometry;
  InitialState* initial_state;
  Source source;
  double factor;
  InputData();
  ~InputData();
  void load(fstream& file,bool init=false);
  void save(fstream& file);
  void remove_initial_state();
};



inline
InputData::InputData(){
  initial_state=new InitialState(&geometry);
}

inline InputData::~InputData(){
  remove_initial_state();
}

inline void
InputData::remove_initial_state() {
  if(initial_state!=nullptr){
    delete initial_state;
  }
}


#endif
