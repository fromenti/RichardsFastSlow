#include "time.hpp"

double Time::T=10;
size_t Time::nT=1000;
double Time::dT=Time::T/(Time::nT-1);

void
Time::save(fstream& file){
  file.write((char*)&T,sizeof(double));
  file.write((char*)&nT,sizeof(size_t));
}

void
Time::load(fstream& file){
  file.read((char*)&T,sizeof(double));
  file.read((char*)&nT,sizeof(size_t));
  dT=T/(nT-1);
}
