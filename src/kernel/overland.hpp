#ifndef OVERLAND_HPP
#define OVERLAND_HPP

#include "geometry.hpp"
#include "physics.hpp"

namespace Kernel{
  class Overland{
  private:
    const Geometry* geometry;
    double* flux_soil;
    void compute_flux_soil();
    void apply_flow();
    void runoff_to_local_min();
    void basin_transfer();

  public:
    //Input
    double dt;
    double* init_hov;
    double** P;
    double* l;
    double* Pl;
    double* hydr;
    double n1_init_hov;
    double* previous_hov;

    //Output
    double* Psoil; //Own by Overland object
    double* hov;
    double* error_x;
    double total_error;

    Overland();
    void init(const Geometry* geometry);
    void run();
  };

  inline
  Overland::Overland(){
  }

  inline void
  Overland::init(const Geometry* geometry_){
    geometry=geometry_;
    Psoil=new double[geometry->nX];
    flux_soil=new double[geometry->nX];
    hov=new double[geometry->nX];
  }


}
#endif
