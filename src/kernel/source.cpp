#include "source.hpp"

Pump::Pump(){
  amplitude_init=1.e-4;
  left_init=0.45;
  right_init=0.55;
  bottom_init=0.45;
  top_init=0.55;
  delta_left_init=0.05;
  delta_right_init=0.05;
  delta_top_init=0.05;
  delta_bottom_init=0.05;
  amplitude_final=1.e-4;
  left_final=0.45;
  right_final=0.55;
  bottom_final=0.45;
  top_final=0.55;
  delta_left_final=0.05;
  delta_right_final=0.05;
  delta_top_final=0.05;
  delta_bottom_final=0.05;
}

void
Pump::save(fstream& file){
  file.write((char*)&amplitude_init,sizeof(double));
  file.write((char*)&left_init,sizeof(double));
  file.write((char*)&right_init,sizeof(double));
  file.write((char*)&bottom_init,sizeof(double));
  file.write((char*)&top_init,sizeof(double));
  file.write((char*)&delta_left_init,sizeof(double));
  file.write((char*)&delta_right_init,sizeof(double));
  file.write((char*)&delta_bottom_init,sizeof(double));
  file.write((char*)&delta_top_init,sizeof(double));
  file.write((char*)&amplitude_final,sizeof(double));
  file.write((char*)&left_final,sizeof(double));
  file.write((char*)&right_final,sizeof(double));
  file.write((char*)&bottom_final,sizeof(double));
  file.write((char*)&top_final,sizeof(double));
  file.write((char*)&delta_left_final,sizeof(double));
  file.write((char*)&delta_right_final,sizeof(double));
  file.write((char*)&delta_bottom_final,sizeof(double));
  file.write((char*)&delta_top_final,sizeof(double));
}

void
Pump::load(fstream& file){
  file.read((char*)&amplitude_init,sizeof(double));
  file.read((char*)&left_init,sizeof(double));
  file.read((char*)&right_init,sizeof(double));
  file.read((char*)&bottom_init,sizeof(double));
  file.read((char*)&top_init,sizeof(double));
  file.read((char*)&delta_left_init,sizeof(double));
  file.read((char*)&delta_right_init,sizeof(double));
  file.read((char*)&delta_bottom_init,sizeof(double));
  file.read((char*)&delta_top_init,sizeof(double));
  file.read((char*)&amplitude_final,sizeof(double));
  file.read((char*)&left_final,sizeof(double));
  file.read((char*)&right_final,sizeof(double));
  file.read((char*)&bottom_final,sizeof(double));
  file.read((char*)&top_final,sizeof(double));
  file.read((char*)&delta_left_final,sizeof(double));
  file.read((char*)&delta_right_final,sizeof(double));
  file.read((char*)&delta_bottom_final,sizeof(double));
  file.read((char*)&delta_top_final,sizeof(double));
}

double
Pump::get_amplitude(double t){
  return (1-t)*amplitude_init+t*amplitude_final;
}

double
Pump::get_left(double t){
  return (1-t)*left_init+t*left_final;
}

double
Pump::get_right(double t){
  return (1-t)*right_init+t*right_final;
}

double
Pump::get_top(double t){
  return (1-t)*top_init+t*top_final;
}

double
Pump::get_bottom(double t){
  return (1-t)*bottom_init+t*bottom_final;
}

double
Pump::get_left_delta(double t){
  return (1-t)*delta_left_init+t*delta_left_final;
}

double
Pump::get_right_delta(double t){
  return (1-t)*delta_right_init+t*delta_right_final;
}

double
Pump::get_top_delta(double t){
  return (1-t)*delta_top_init+t*delta_top_final;
}

double
Pump::get_bottom_delta(double t){
  return (1-t)*delta_bottom_init+t*delta_bottom_final;
}

double
Pump::value(double x,double z,double t){
  double amplitude=interpol(amplitude_init,amplitude_final,t);
  double delta_left=interpol(delta_left_init,delta_left_final,t);
  double left=interpol(left_init,left_final,t);
  double delta_right=interpol(delta_right_init,delta_right_final,t);
  double right=interpol(right_init,right_final,t);
  double delta_top=interpol(delta_top_init,delta_top_final,t);
  double top=interpol(top_init,top_final,t);
  double delta_bottom=interpol(delta_bottom_init,delta_bottom_final,t);
  double bottom=interpol(bottom_init,bottom_final,t);
  double fx=bump(x,delta_left,left,delta_right,right);
  double fz=bump(z,delta_bottom,bottom,delta_top,top);
  return amplitude*fx*fz;
}

Cloud::Cloud(){
  amplitude_init=1.e-4;
  left_init=0.45;
  right_init=0.55;
  delta_left_init=0.05;
  delta_right_init=0.05;
  amplitude_final=1.e-4;
  left_final=0.45;
  right_final=0.55;
  delta_left_final=0.05;
  delta_right_final=0.05;
}


void
Cloud::save(fstream& file){
  file.write((char*)&amplitude_init,sizeof(double));
  file.write((char*)&left_init,sizeof(double));
  file.write((char*)&right_init,sizeof(double));
  file.write((char*)&delta_left_init,sizeof(double));
  file.write((char*)&delta_right_init,sizeof(double));
  file.write((char*)&amplitude_final,sizeof(double));
  file.write((char*)&left_final,sizeof(double));
  file.write((char*)&right_final,sizeof(double));
  file.write((char*)&delta_left_final,sizeof(double));
  file.write((char*)&delta_right_final,sizeof(double));
}

void
Cloud::load(fstream& file){
  file.read((char*)&amplitude_init,sizeof(double));
  file.read((char*)&left_init,sizeof(double));
  file.read((char*)&right_init,sizeof(double));
  file.read((char*)&delta_left_init,sizeof(double));
  file.read((char*)&delta_right_init,sizeof(double));
  file.read((char*)&amplitude_final,sizeof(double));
  file.read((char*)&left_final,sizeof(double));
  file.read((char*)&right_final,sizeof(double));
  file.read((char*)&delta_left_final,sizeof(double));
  file.read((char*)&delta_right_final,sizeof(double));
}

double
Cloud::get_amplitude(double t){
  return (1-t)*amplitude_init+t*amplitude_final;
}

double
Cloud::get_amplitude_max(){
  return max(amplitude_init,amplitude_final);
}

double
Cloud::get_left(double t){
  return (1-t)*left_init+t*left_final;
}

double
Cloud::get_right(double t){
  return (1-t)*right_init+t*right_final;
}

double
Cloud::get_left_delta(double t){
  return (1-t)*delta_left_init+t*delta_left_final;
}

double
Cloud::get_right_delta(double t){
  return (1-t)*delta_right_init+t*delta_right_final;
}

Source::~Source(){
  for(auto it=pumps.begin();it!=pumps.end();++it){
    delete *it;
  }
  for(auto it=clouds.begin();it!=clouds.end();++it){
    delete *it;
  }
}

Pump*
Source::addPump(){
  Pump* pump=new Pump;
  pumps.push_back(pump);
  return pump;
}

void
Source::removePump(Pump* pump){
  for(auto it=pumps.begin();it!=pumps.end();++it){
    if(*it==pump){
      delete *it;
      pumps.erase(it);
      return;
    }
  }
}

Cloud*
Source::addCloud(){
  Cloud* cloud=new Cloud;
  clouds.push_back(cloud);
  return cloud;
}

void
Source::removeCloud(Cloud* cloud){
  for(auto it=clouds.begin();it!=clouds.end();++it){
    if(*it==cloud){
      delete *it;
      clouds.erase(it);
      return;
    }
  }
}

void
Source::save(fstream& file){
  size_t n=pumps.size();
  file.write((char*)&n,sizeof(double));
  for(auto it=pumps.begin();it!=pumps.end();++it){
    (*it)->save(file);
  }
  n=clouds.size();
  file.write((char*)&n,sizeof(double));
  for(auto it=clouds.begin();it!=clouds.end();++it){
    (*it)->save(file);
  }
}

void
Source::load(fstream& file){
  size_t n;
  file.read((char*)&n,sizeof(size_t));
  for(size_t i=0;i<n;++i){
    Pump* pump=addPump();
    pump->load(file);
  }
  file.read((char*)&n,sizeof(size_t));
  for(size_t i=0;i<n;++i){
    Cloud* cloud=addCloud();
    cloud->load(file);
  }
}
