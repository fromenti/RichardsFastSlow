#ifndef PHYSICS_HPP
#define PHYSICS_HPP

#include <cmath>
#include <cassert>
#include <iostream>
#include <fstream>

using namespace std;


//! The Physics class contains all physical parameters characterising the soil.
//! This class contains only static members.
class Physics{
public:
  static constexpr double minimal_height_to_runoff=0.019;

  enum Model{BrooksCorey};

  //! Set physics model
  static void setModel(Model model);

  //! Gravity acceleration (m.s^-2)
  static double g;
  //! Fluid density (g.l^(-1))
  static double rho;

  //! Porosity of the soil
  static double phi;
  //! Conductivity of the saturated soil
  static double k0;

  static double Psec;
  //! Characterise the water pressure at the bottom of the overland water
  static double nivrivsat;

  //! Model used
  static Model model;

  //! Return the saturation in function of the pressure
  static double (*s)(double);
  //! Return the derivtive of the saturation in function of the pressure
  static double (*ds)(double);
  //! Set the saturation and its derivative in function of the pressure
  static void (*s_ds)(double,double&,double&);

  static double (*s_inv)(double);

  //! Return the relative conductivity in function of the pressure
  static double (*kr)(double);
  //! Return the derivtive of the relative conductivity in function of the pressure
  static double (*dkr)(double);
  //! Set the relative conductivity and its derivative in function of the pressure
  static void (*kr_dkr)(double,double&,double&);

  //---------------------
  // Models descriptions
  //---------------------

  //! Datas used to define the model
  static double model_data[6];

  //! Max model Datas
  static const size_t max_model_parameters=6;

  //------------------------
  // Brooks and Corey model
  //------------------------

  //model_data[0] -> psat : minimal pressure such that s(psat)=1
  //model_data[1] -> sres : residual pressure

  //model_data[2] -> lambda
  //model_data[3] -> alpha

  //! Brooks and Corey saturation map
  static double s_BC(double P);
  //! Brooks and Corey derivative of the saturation map
  static double ds_BC(double P);
  //! Brooks and Corey saturation and its derivative setter
  static void (s_ds_BC)(double P,double& v,double& dv);

  static double s_inv_BC(double s);

  //! Brooks and Corey relative conductivity map
  static double kr_BC(double P);
  //! Brooks and Corey derivative of the relative conductivity map
  static double dkr_BC(double P);
  //! Brooks and Corey relative conductivity and its derivative setter
  static void (kr_dkr_BC)(double P,double& v,double& dv);

  static double Psoil(double hsoil,double hov);
  static double dPsoil(double hsoil,double hov);
  static double invPsoil(double hsoil,double Psol);

  static double tilde_a(double l,double hbot);
  static double tilde_c (double l,double hbot);

  //--------------------
  // File input/output
  //--------------------

  static void save(fstream& file);
  static void load(fstream& file);

};

#define Psat Physics::model_data[0]
#endif
