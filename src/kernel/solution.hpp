#ifndef SOLUTION_HPP
#define SOLUTION_HPP

#include "geometry.hpp"

class Solution{
public:
  double** P;
  double* hydr;
  double* hov;
  double* hsat;
  double* l;
  double* Pl;
  double mass;
  size_t nstep_Piccard;

  Solution(const Geometry& geometry);

};



#endif
