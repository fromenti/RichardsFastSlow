#include "input_data.hpp"

void
InputData::load(fstream& file,bool init){
  Physics::load(file);
  Time::load(file);
  geometry.load(file,init);
  initial_state->load(file,init);
  source.load(file);
  file.read((char*)&factor,sizeof(double));
}

void
InputData::save(fstream& file){
  Physics::save(file);
  Time::save(file);
  geometry.save(file);
  initial_state->save(file);
  source.save(file);
  file.write((char*)&factor,sizeof(double));
}
