#ifndef BASIN_HPP
#define BASIN_HPP

#include <string>
#include "physics.hpp"
#include "debug.hpp"

using namespace std;

enum Direction{None,Left,Right,Both};
enum BasinPosition{Middle,BorderLeft,BorderRight,BorderBoth};

struct Summit{
  size_t ix;
  double h;
  bool operator<(const Summit& S) const;
  Summit(size_t ix,double h);
};


class Basin{
public:
  static constexpr double delta_both_leak=0.01;
  bool overflow;
  size_t xleft,xright;
  size_t xmax_left,xmax_right;
  size_t xacc;
  Direction leak_direction;
  BasinPosition position;
  double hmin,hleft,hright,hmax;
  Basin* left;
  Basin* right;
  double vmin;
  double vflow;
  double vmax;
  double vleak;
  void compute_leak_direction();
  void compute_maxs(double* hsoil);
  void compute_vflow(double* hsoil,double* hov);
  void repartition(double* hsoil,double* hov,Direction* rundir);
  void runoff(size_t xstart,Direction dir,Direction* rundir,double* hsoil,double* hov);

  double f(double r,double* hsoil,double* hov,double vext);
  double df(double r,double* hsoil,double* hov,double vext);
public:
  Basin();
  Basin(Basin* left,Basin* right);
  void display(string indent="");
  bool is_primitive();
    void flatten(double* hsoil,double* hov);
};

inline
Summit::Summit(size_t ix_,double h_):ix(ix_),h(h_){
}

inline bool
Summit::operator<(const Summit& S) const{
  return h<S.h;
}

inline Basin::Basin(){
  left=nullptr;
  right=nullptr;
  position=Middle;
}

inline bool
Basin::is_primitive(){
  return left==nullptr;
}


#endif
