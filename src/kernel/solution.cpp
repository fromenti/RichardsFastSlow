#include "solution.hpp"

Solution::Solution(const Geometry& geometry){
  size_t nX=geometry.nX;
  P=new double*[nX];
  for(size_t ix=0;ix<nX;++ix){
    P[ix]=new double[geometry.nZ[ix]];
  }
  hydr=new double[nX];
  hov=new double[nX];
  hsat=new double[nX];
  l=new double[nX];
  Pl=new double[nX];
}
