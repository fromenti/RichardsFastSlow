#ifndef ALL_VERTICAL_RICHARDS_HPP
#define ALL_VERTICAL_RICHARDS_HPP

#include "geometry.hpp"
#include "richards_evolutive_time.hpp"

namespace Kernel{
  class AllVerticalRichards{
  private:
    static constexpr double max_error_x=1e-8;
    const Geometry* geometry;
    double** pumps;
    bool* indice_x_Richards;
    RichardsEvolutiveTime* richards_evolutive_time;

  public:
    //Input
    double dt;
    double* error_x;
  
    double** init_P; //P_0
    double** previous_P; //P_{k-1}
    double* hydr;
    double* l;
    double* Pl;
    double* Psoil;
    //Output
    double** P; //P_k
    double* hsat;
    bool has_converged;

    AllVerticalRichards();
    void init(const Geometry* geometry,double** pumps);
    void init_indice_x_Richards();
    void update_indice_x_Richards();
    void compute_hsat();
    void run();
  };

  inline
  AllVerticalRichards::AllVerticalRichards(){
    richards_evolutive_time=nullptr;
  }


}
#endif
