#ifndef TIME_HPP
#define TIME_HPP

#include <fstream>
#include <iostream>

using namespace std;

class Time{
public:
  //! Total duration of the simulation
  static double T;

  //! Number of time steps
  static size_t nT;

  //! Time step
  static double dT;

  //! Save time in file
  static void save(fstream& file);

  //! Load time from file
  static void load(fstream& file);
};
#endif
