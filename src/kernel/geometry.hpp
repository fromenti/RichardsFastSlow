#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP

#include <iostream>
#include <fstream>
#include <limits>
#include <list>
#include <vector>
#include <algorithm>

#include "math/spline.hpp"
#include "basin.hpp"
using namespace std;

using Func = double (*)(double);

extern double inf;



//! The Geometry class contains all geometric parameters of the domain.
class Geometry{
public:
  static constexpr double max_slope_both_side_runoff=0.00;
  //! Horizontal length of the domain
  double lX;

  //! Number of horizontal steps
  size_t nX;

  //! Horizontal step
  double dX;

  //! Level of the soil depending on X, e.g, hsoil[k]=level of the soil at X=k*dX.
  //! Vector of size nX.
  double* hsoil;

  //! Derivative of the soil depending on X, vector of size nX.
  double* dhsoil;

  //! Level of the bottom depending on X, e.g, hbot[k]=level of the bottom at X=k*dX.
  //! Vector of size nX.
  double* hbot;

  //! Derivative of the bottom depending on X, vector of size nX.
  double* dhbot;


  //! Number of vertical step at a given X, vector of size nX.
  size_t* nZ;
  //! Vertical step at a given X, vector of size nX.
  double* dZ;

  //! Vertical considered positions at a given X, vector of vectors of size nX. For each k, Z[k] is a vector of size nZ[k]
  double** Z;

  Direction* runoff_directions;

  Basin* root_basin;
  //!
  Geometry();

  ~Geometry();

  void initZ(double dZ_avg,bool init=true);

  void save(fstream& file);

  void load(fstream& file,bool init);

  pair<list<Basin*>::iterator,list<Basin*>::iterator> find_basins(const Summit& s,list<Basin*>& basins);

  void compute_basins();
};


#endif
