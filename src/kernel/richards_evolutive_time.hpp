#ifndef RICHARDS_EVOLUTIVE_TIME_HPP
#define RICHARDS_EVOLUTIVE_TIME_HPP

#include "geometry.hpp"
#include "richards.hpp"

namespace Kernel{
  class RichardsEvolutiveTime{
  private:
    static constexpr size_t max_Richards_time_subdivisions=256;
    size_t ix;
    size_t nZ;
    double* Z;
    double dX;

    double flux_bot;
    double* div_w;
    double* pumps;
    
    void compute_flux_bot();
    void compute_div_w();
    Richards richards;
  public:
    //Input
    double dt;

    double* init_P;
    double* previous_P; //relatively to k

    double hydr_left;
    double l_left;
    double Pl_left;

    double hydr_middle;
    double l_middle;
    double Pl_middle;

    double hydr_right;
    double l_right;
    double Pl_right;

    //Output
    double* P;
    double hsat;
    bool has_converged;

    RichardsEvolutiveTime();
    void init(size_t ix,const Geometry* geometry,double* pumps);
    void run();
  };

  inline
  RichardsEvolutiveTime::RichardsEvolutiveTime(){

  }
}
#endif
