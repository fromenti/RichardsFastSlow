#ifndef SOURCE_HPP
#define SOURCE_HPP

#include <list>
#include <fstream>
#include <iostream>
#include "math/algo.hpp"

using namespace std;


class Pump{
public:
  double amplitude_init;
  double left_init,right_init,bottom_init,top_init;
  double delta_left_init,delta_right_init,delta_bottom_init,delta_top_init;
  double amplitude_final;
  double left_final,right_final,bottom_final,top_final;
  double delta_left_final,delta_right_final,delta_bottom_final,delta_top_final;
  double get_amplitude(double t);
  double get_left(double t);
  double get_right(double t);
  double get_top(double t);
  double get_bottom(double t);
  double get_left_delta(double t);
  double get_right_delta(double t);
  double get_top_delta(double t);
  double get_bottom_delta(double t);
  Pump();
  void save(fstream& file);
  void load(fstream& file);
  double value(double x,double z,double t);
};

class Cloud{
public:
  double amplitude_init;
  double left_init,right_init;
  double delta_left_init,delta_right_init;
  double amplitude_final;
  double left_final,right_final;
  double delta_left_final,delta_right_final;
  double get_amplitude(double t);
  double get_amplitude_max();
  double get_left(double t);
  double get_right(double t);
  double get_left_delta(double t);
  double get_right_delta(double t);
  Cloud();
  void save(fstream& file);
  void load(fstream& file);

};

class Source{
public:
  list<Pump*> pumps;
  list<Cloud*> clouds;
  Source();
  ~Source();
  Pump* addPump();
  Cloud* addCloud();
  void removePump(Pump* pump);
  void removeCloud(Cloud* cloud);
  void save(fstream& file);
  void load(fstream& file);
};

inline Source::Source(){
}

#endif
