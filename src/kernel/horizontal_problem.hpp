#ifndef HORIZONTAL_PROBLEM_HPP
#define HORIZONTAL_PROBLEM_HPP

#include "geometry.hpp"
#include "physics.hpp"


namespace Kernel{
  /*! Class for Horzontal problem.
   *! Here we code only homogeneus Neumann conditions.
  */
  class HorizontalProblem{
  private:
    const Geometry* geometry;
    double* sup_M;
    double* diag_M;
    double* sub_M;
    double* F;
    void solve_system();
    void compute_error();
  public:
    //Input
    double* previous_hydr;
    double* l;
    double* Pl;
    double n1_init_hydr;
    //Output
    double* hydr;
    double total_error;
    double* error_x;


    HorizontalProblem();

    void init(const Geometry* geometry);

    void run();

    double get_error(size_t ix);
  };

  inline HorizontalProblem::HorizontalProblem(){
  }


}

#endif
