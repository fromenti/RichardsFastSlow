#ifndef INITIAL_STATE_HPP
#define INITIAL_STATE_HPP

#include <list>
#include <fstream>
#include "physics.hpp"
#include "geometry.hpp"
#include "math/spline.hpp"

using namespace std;

class Tank{
public:
  double saturation;
  double left,right,bottom,top;
  double delta_left,delta_right,delta_bottom,delta_top;
  Tank();
  void save(fstream& file);
  void load(fstream& file);
};

class InitialState{
private:
  Geometry* geometry;
public:
  //! Vector of size nX
  double* hsat;


  double* hov;

  //! Vector of vector of size nX. For each k, Pinit[k] is a vector of size geometry->nZ[k]
  double** Pinit;
  list<Tank*> tanks;
  InitialState();
  InitialState(Geometry* geometry);
  ~InitialState();
  Tank* addTank();
  void removeTank(Tank* tank);
  void updatePressure();
  void save(fstream& file);
  void load(fstream& file,bool init);
};

#endif
