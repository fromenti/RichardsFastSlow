#ifndef POINT_HPP
#define POINT_HPP

#include <iostream>

using namespace std;

class Point{
public:
  double x;
  double y;
  Point();
  Point(double x,double y);
};

ostream& operator<<(ostream& os,const Point& P);

inline
Point::Point():x(0),y(0){
}

inline
Point::Point(double _x,double _y):x(_x),y(_y){
}

inline ostream&
operator<<(ostream& os,const Point& P){
  return os<<'('<<P.x<<','<<P.y<<')';
}

#endif
