#ifndef SPLINE_HPP
#define SPLINE_HPP

#include <iostream>
#include <cmath>
#include "math/point.hpp"
#include "math/poly3.hpp"
#include "math/algo.hpp"

using namespace std;


class Spline{
private:
  Poly3* q;
  Point* P;
  size_t n;
  size_t findIndex(double x) const;
public:
  Spline();
  ~Spline();
  void setPoints(Point* points,size_t n);
  void compute();
  double operator()(double x) const;
  double derivate(double x) const;
};

inline Spline::Spline(){
}

inline Spline::~Spline(){
  delete[] q;
}


#endif
