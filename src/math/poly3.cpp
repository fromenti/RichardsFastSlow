#include "poly3.hpp"

double
Poly3::operator()(double x) const{
  double res=a;
  res=res*x+b;
  res=res*x+c;
  res=res*x+d;
  return res;
}

double
Poly3::derivate(double x) const{
  double res=a;
  res=res*x+b;
  res=res*x+c;
  return res;
}
