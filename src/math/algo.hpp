#ifndef ALGO_HPP
#define ALGO_HPP

#include "debug.hpp"

#include <cmath>
#include <iostream>
#include <string>

using namespace std;
/*! Thomas algorithm to solve AX=B where A is a tridiagonal matrix
  \param n size of the matrix A
  \param a (n-1) sub-diagonal coefficients of matrix A
  \param b n diagonal coefficients of matrix A
  \param c (n-1) sup-diagonal coefficients of matrix A (modified)
  \param d n coefficients of vector B (modified)
  \param x n coefficients vector to store the result (modified)
*/

void Thomas(size_t n,double* a,double* b,double* c,double* d,double* x);
double norm1(double* u,size_t n);
double norm2(double* u,size_t n);
double error2(double* u,double* v,size_t n);
void clear(double* u,size_t n);
void display(string name,double* u,size_t n);
double bump(double x,double delta_left,double left,double delta_right,double right);
double interpol(double x0,double x1,double t);

#endif
