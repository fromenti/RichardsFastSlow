#ifndef POLY3_HPP
#define POLY3_HPP

#include <iostream>

using namespace std;

class Poly3{
public:
  double a,b,c,d;
  Poly3();
  Poly3(double a,double b,double c,double d);
  double operator()(double x) const;
  double derivate(double x) const;
};

ostream& operator<<(ostream& os,const Poly3& P);

inline
Poly3::Poly3(){
}

inline
Poly3::Poly3(double _a,double _b,double _c,double _d):a(_a),b(_b),c(_c),d(_d){
}

inline
ostream& operator<<(ostream& os,const Poly3& P){
  return os<<'['<<P.a<<','<<P.b<<','<<P.c<<','<<P.d<<']';
}
#endif
