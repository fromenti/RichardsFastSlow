#include <iostream>
#include <QApplication>
#include "qt/mainwindow.hpp"
#include "physics.hpp"
#include "geometry.hpp"

using namespace std;



int main(int argc,char** argv){
  QApplication app(argc,argv);
  QtMainWindow* window;
  if(argc==2){
    window=new QtMainWindow(argv[1]);
  }
  else{
    window=new QtMainWindow;
  }
  window->resize(1280,1024);
  window->show();
  return app.exec();

}
