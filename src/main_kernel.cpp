#include <iostream>
#include "kernel/solver.hpp"


using namespace std;

int main(int argc,char** argv){
  Kernel::Solver solver("inputs/kernel-2.input");
  for(size_t i=0;i<Time::nT;++i){
    cout<<i<<" of "<<Time::nT<<endl;
    solver.next();
  }
}
