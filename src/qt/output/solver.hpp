#ifndef QT_KERNEL_HPP
#define QT_KERNEL_HPP

#include <QThread>
#include <QTime>
#include "../../kernel/solver.hpp"

class QtSolver:public QObject,public Kernel::Solver{
  Q_OBJECT
public:
  QtSolver(string filename);
  ~QtSolver();
public slots:
  void run();
signals:
  void finished(int);
};

inline
QtSolver::~QtSolver(){
}


#endif
