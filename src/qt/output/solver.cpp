#include "qt/output/solver.hpp"

QtSolver::QtSolver(string filename):Kernel::Solver(filename){
}

void
QtSolver::run(){
  QTime start=QTime::currentTime();
  cout<<"[QtSolver::run] start"<<endl;
  for(size_t i=1;i<Time::nT;++i){
    bool has_converged=compute_next_solution();
    if(!has_converged){
      cout<<"Does not converge ... Game Over"<<endl;
      break;
    }
  }
  cout<<"[QtSolver::run] end"<<endl;
  QTime end=QTime::currentTime();
  float ms=start.msecsTo(end);
  cout<<"[QtSolver::run] duration : "<<ms/1000<<endl;

}
