#ifndef QT_MAINWINDOW_HPP
#define QT_MAINWINDOW_HPP

#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QApplication>

#include "qt/output/output.hpp"
#include "qt/output/solver.hpp"

class QtMainWindow:public QMainWindow{
  Q_OBJECT
private:
  QtOutput* output;
public:
  QtMainWindow();
  ~QtMainWindow();
};

inline
QtMainWindow::~QtMainWindow(){
}
#endif
