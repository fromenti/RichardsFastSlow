#include "qt/output/output.hpp"

QtOutput::QtOutput(string filename):QWidget(){

  solver=new QtSolver(filename);
  solver_thread=new QThread();
  solver->moveToThread(solver_thread);

  main_layout=new QVBoxLayout;
  time_bar=new QScrollBar(Qt::Horizontal);
  output_view=new QtOutputView(solver);
  info_widget=new QWidget;
  info_layout=new QGridLayout;
  time_label=new QLabel;

  time_bar->setMinimum(0);
  time_bar->setMaximum(Time::nT-1);
  info_layout->addWidget(time_label);
  info_widget->setLayout(info_layout);
  main_layout->addWidget(output_view,1);
  main_layout->addWidget(time_bar);
  main_layout->addWidget(info_widget);
  setLayout(main_layout);
  step=0;
  update();
  connect(time_bar,&QScrollBar::valueChanged,this,&QtOutput::time_change);
  connect(solver_thread,&QThread::started,solver,&QtSolver::run);
  solver_thread->start();
}

QtOutput::~QtOutput(){
}

void
QtOutput::update(){
  update_infos();
  output_view->draw(step);
}

void
QtOutput::update_infos(){
  time_label->setText("Time : "+QString::number(step*Time::dT));
}

void
QtOutput::time_change(int val){
  step=val;
  update();
}
