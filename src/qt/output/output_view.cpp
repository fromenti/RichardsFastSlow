#include "qt/output/output_view.hpp"

QtOutputView::QtOutputView(QtSolver* k){
  solver=k;
  nX=solver->get_geometry().nX;
  dX=solver->get_geometry().dX;
  lX=solver->get_geometry().lX;
  factor=solver->get_vertical_factor();
};

void
QtOutputView::initializeGL(){
  glClearColor(1,1,1,1);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHT0);
  glEnable(GL_LIGHTING);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glEnable(GL_COLOR_MATERIAL);
}

void
QtOutputView::paintGL(){
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  if(step>=solver->get_computed_solutions_number()) return;
  double h=height();
  double w=width();
  glBegin(GL_TRIANGLES);
  for(size_t i=0;i<nX-1;++i){
    size_t max_left=solver->get_geometry().nZ[i];
    size_t max_right=solver->get_geometry().nZ[i+1];
    size_t left=0;
    size_t right=0;
    while(left<max_left-1 and right<max_right-1){
      if(solver->get_geometry().Z[i][left+1]<=solver->get_geometry().Z[i+1][right+1]){
        drawTriangle(i,left,i+1,right,i,left+1);
        ++left;
      }
      else{
        drawTriangle(i,left,i+1,right,i+1,right+1);
        ++right;
      }
    }
    if(left==max_left-1){
      while(right<max_right-1){
        drawTriangle(i,left,i+1,right,i+1,right+1);
        ++right;
      }
    }
    if(right==max_right-1){
      while(left<max_left-1){
        drawTriangle(i,left,i+1,right,i,left+1);
        ++left;
      }
    }
  }
  glEnd();
  //Draw area under hbot
  glColor3f(0.6,0.6,0.6);
  glLineWidth(3);
  glBegin(GL_QUADS);
  for(size_t ix=0;ix<nX-1;ix+=1){
    double x=ix*dX;
    glVertex3f(x,0,1);
    glVertex3f(x,solver->get_geometry().hbot[ix],0);
    glVertex3f(x+dX,solver->get_geometry().hbot[ix+1],0);
    glVertex3f(x+dX,0,0);

  }
  glEnd();

  //Draw hsoil
  glColor3f(0.6,0.3,0);
  glLineWidth(1);
  glBegin(GL_LINE_STRIP);
  for(size_t ix=0;ix<nX;ix+=1){
    double x=ix*dX;
    glVertex3f(x,solver->get_geometry().hsoil[ix],0.8);
  }
  glEnd();

  //Draw hydr
  glColor3f(1,0,0);
  glLineWidth(3);
  glBegin(GL_LINE_STRIP);
  for(size_t ix=0;ix<nX;ix+=1){
    double x=ix*dX;
    glVertex3f(x,solver->get_solution(step)->hydr[ix]-Psat/(Physics::rho*Physics::g),1);
  }
  glEnd();

  //Draw hov
  glColor3f(0,0,0);
  glLineWidth(1);
  glBegin(GL_LINE_STRIP);
  for(size_t ix=0;ix<nX;ix+=1){
    double x=ix*dX;
    double y=solver->get_solution(step)->hov[ix];
    glVertex3f(x,y,1);
  }
  glEnd();

  //Draw hsat
  glColor3f(0,1,1);
  glLineWidth(1);
  glBegin(GL_LINE_STRIP);
  for(size_t ix=0;ix<nX;ix+=1){
    double x=ix*dX;
    double y=solver->get_solution(step)->hsat[ix];
    glVertex3f(x,y,1);
  }
  glEnd();

  //Draw pumps
  for(auto it=solver->get_source().pumps.begin();it!=solver->get_source().pumps.end();++it){
    drawPump(*it);
  }

drawOverland();
  drawVectors();
}

void
QtOutputView::resizeGL(int w,int h){
  glViewport(0,0,w,h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0,lX,0,factor);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void
QtOutputView::drawTriangle(size_t ix1,size_t iz1,size_t ix2,size_t iz2,size_t ix3,size_t iz3){
  double factor_inv=1;//1/solver->solution.factor;
  double h=height();
  double w=width();
  double x1=ix1*dX;
  double x2=ix2*dX;
  double x3=ix3*dX;
  double y1=solver->get_geometry().Z[ix1][iz1]*factor_inv;;
  double y2=solver->get_geometry().Z[ix2][iz2]*factor_inv;;
  double y3=solver->get_geometry().Z[ix3][iz3]*factor_inv;;
  setColor(ix1,iz1);
  glVertex3f(x1,y1,0);
  setColor(ix2,iz2);
  glVertex3f(x2,y2,0);
  setColor(ix3,iz3);
  glVertex3f(x3,y3,0);
}

void
QtOutputView::draw(size_t _step){
  step=_step;
  update();
}

void
QtOutputView::mousePressEvent(QMouseEvent* event){
  double x=double(event->x())/width()*lX;
  double y=1-(double(event->y())/height());
  displayInfos(x,y*factor);
}

void
QtOutputView::setColor(size_t ix,size_t iz){
  double p=getP(ix,iz);
  double s=Physics::s(p);

  double r=(1-s);
  double g=0.7*(1-s);
  double b=0.8*s+0.4*(1-s);
  glColor3f(r,g,b);
}

void
QtOutputView::drawVector(size_t ix,size_t iz,double u,double v){
  double h=height();
  double w=width();

  double x1=ix*dX;
  double y1=solver->get_geometry().Z[ix][iz];
  double x2=x1+u;
  double y2=y1+v;
  double x3=x2+(-0.866*u/lX*w+0.5*v/factor*h)*scale_arrow_head*lX/w;//*lX;
  double y3=y2+(-0.5*u/lX*w-0.866*v/factor*h)*scale_arrow_head*factor/h;

  double x4=x2+(-0.866*u/lX*w-0.5*v/factor*h)*scale_arrow_head*lX/w;//*lX;
  double y4=y2+(0.5*u/lX*w-0.866*v/factor*h)*scale_arrow_head*factor/h;

  glColor3f(1,1,0);
  glBegin(GL_LINE_STRIP);
  glVertex3f(x1,y1,0.8);
  glVertex3f(x2,y2,0.8);
  glEnd();
  glBegin(GL_LINE_STRIP);
  glVertex3f(x3,y3,0.8);
  glVertex3f(x2,y2,0.8);
  glVertex3f(x4,y4,0.8);
  glEnd();

}

void
QtOutputView::drawVectors(){
  for(size_t ix=1;ix<nX-1;ix+=arrow_step_x){
    //Case iz=0
    double p1=getP(ix,0);
    double p2=getP(ix,1);
    double u=-Physics::k0*Physics::kr(getPl(ix)+Physics::rho*Physics::g*(getl(ix)-solver->get_geometry().Z[ix][0]))*(getHydr(ix+1)-getHydr(ix-1))/(2*dX);
    double v=-Physics::k0*Physics::kr(p1)*((p2-p1)/(Physics::rho*Physics::g*solver->get_geometry().dZ[ix])+1);
    drawVector(ix,0,scale_arrow*u,scale_arrow*v);
    for(size_t iz=arrow_step_z;iz<solver->get_geometry().nZ[ix]-1;iz+=arrow_step_z){
      double p0=getP(ix,iz-1);
      double p1=getP(ix,iz);
      double p2=getP(ix,iz+1);
      double u=-Physics::k0*Physics::kr(getPl(ix)+Physics::rho*Physics::g*(getl(ix)-solver->get_geometry().Z[ix][iz]))*(getHydr(ix+1)-getHydr(ix-1))/(2*dX);
      double v=-Physics::k0*Physics::kr(p1)*((p2-p0)/(2*Physics::rho*Physics::g*solver->get_geometry().dZ[ix])+1);
      drawVector(ix,iz,scale_arrow*u,scale_arrow*v);
    }
  }
}

void
QtOutputView::drawPump(Pump* pump){
  double time=(step*Time::dT)/Time::T;

  double a=pump->get_amplitude(time);

  if(a==0) return;
  double l=pump->get_left(time);
  double r=pump->get_right(time);
  double b=pump->get_bottom(time);//*factor_inv;
  double t=pump->get_top(time);//*factor_inv;
  double dl=pump->get_left_delta(time);
  double dr=pump->get_right_delta(time);
  double db=pump->get_bottom_delta(time);//*factor_inv;
  double dt=pump->get_top_delta(time);//*factor_inv;
  glLineWidth(2);
  if(a>0) glColor3f(0,0.6,1);
  else glColor3f(1,0.2,0);
  glLineStipple(3, 0xAAAA);
  glBegin(GL_LINE_LOOP);
  glVertex3f(l,b,1);
  glVertex3f(r,b,1);
  glVertex3f(r,t,1);
  glVertex3f(l,t,1);
  glEnd();
  glLineWidth(1);
  glEnable(GL_LINE_STIPPLE);
  glBegin(GL_LINE_LOOP);
  glVertex3f(l-dl,b-db,1);
  glVertex3f(r+dr,b-db,1);
  glVertex3f(r+dr,t+dt,1);
  glVertex3f(l-dl,t+dt,1);
  glEnd();
  glDisable(GL_LINE_STIPPLE);
}


void
QtOutputView::drawOverland(){
  const Geometry& G=solver->get_geometry();
  double dX=G.dX;
  glColor3f(0.5,0.5,1);
  glBegin(GL_QUADS);
  for(size_t i=0;i<G.nX-1;++i){
    glVertex3f(i*dX,G.hsoil[i],0);
    glVertex3f(i*dX,solver->get_solution(step)->hov[i],0);
    glVertex3f(i*dX+dX,solver->get_solution(step)->hov[i+1],0);
    glVertex3f(i*dX+dX,G.hsoil[i+1],0);
  }
  glEnd();
}

void
QtOutputView::displayInfos(double x,double z){
  cout<<"---------------------------------"<<endl;
  cout<<"step = "<<step<<endl;
  cout<<"x = "<<x<<endl;
  cout<<"z = "<<z<<endl;
  size_t ix=floor(x/dX+0.5);
  if(ix==nX) --ix;
  cout<<"ix = "<<ix<<endl;

  double hbot=solver->get_geometry().hbot[ix];
  cout<<"hbot = "<<hbot<<endl;
  double hsoil=solver->get_geometry().hsoil[ix];
  cout<<"hsoil = "<<hsoil<<endl;
  cout<<"l = "<<getl(ix)<<endl;
    if(z>hbot and z<hsoil){
    double dZ=solver->get_geometry().dZ[ix];
    cout<<"dZ = "<<dZ<<endl;
    size_t nz=solver->get_geometry().nZ[ix];
    cout<<"nZ = "<<nz<<endl;
    size_t iz=floor((z-hbot)/dZ+0.5);
    if(iz==nz) --iz;
    cout<<"iz = "<<iz<<endl;
    cout<<"P = "<<getP(ix,iz)<<endl;
    }
}
