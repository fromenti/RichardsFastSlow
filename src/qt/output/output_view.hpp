#ifndef QT_OUTPUT_VIEW_HPP
#define QT_OUTPUT_VIEW_HPP

#include <QWidget>
#include <QPainter>
#include <QOpenGLWidget>
#include <QMouseEvent>
#include <GL/glut.h>
#include "qt/output/solver.hpp"


class QtOutputView:public QOpenGLWidget{
private:
  static constexpr size_t const arrow_step_x=3;
  static constexpr size_t const arrow_step_z=7;
  static constexpr double const scale_arrow=12000;
  static constexpr double const scale_arrow_head=0.3;

  size_t nX;
  double dX,lX;
  double factor;
  QtSolver* solver;
  size_t step;
public:
  QtOutputView(QtSolver* solver);
  ~QtOutputView();
  void initializeGL();
  void resizeGL(int x,int h);
  void paintGL();
  double getP(size_t ix,size_t iz);
  double getPl(size_t ix);
  double getl(size_t ix);
  double getHydr(size_t ix);
  void setColor(size_t ix,size_t iz);
  /*  void paintEvent(QPaintEvent* event);*/
  void draw(size_t t);
  void drawTriangle(size_t ix1,size_t iz1,size_t ix2,size_t iz2,size_t ix3,size_t iz3);
  void mousePressEvent(QMouseEvent* event);
  void displayInfos(double x,double y);
  void drawVector(size_t ix,size_t iz,double u,double v);
  void drawVectors();
  void drawPump(Pump* pump);
  void drawOverland();
};

inline
QtOutputView::~QtOutputView(){
}

inline double
QtOutputView::getP(size_t ix,size_t iz){
  return solver->get_solution(step)->P[ix][iz];
}

inline double
QtOutputView::getPl(size_t ix){
  return solver->get_solution(step)->Pl[ix];
}

inline double
QtOutputView::getl(size_t ix){
  return solver->get_solution(step)->l[ix];
}

inline double
QtOutputView::getHydr(size_t ix){
  return solver->get_solution(step)->hydr[ix];
}
#endif
