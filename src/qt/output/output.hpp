#ifndef QT_OUTPUT_HPP
#define QT_OUTPUT_HPP

#include <QWidget>
#include <QVBoxLayout>
#include <QScrollBar>
#include <QGridLayout>
#include <QLabel>
#include <QThread>

#include "qt/output/output_view.hpp"
#include "qt/output/solver.hpp"

class QtOutput:public QWidget{
  Q_OBJECT
private:
  QtSolver* solver;
  QThread* solver_thread;

  QVBoxLayout* main_layout;
  QScrollBar* time_bar;
  QWidget* info_widget;
  QGridLayout* info_layout;
  QLabel* time_label;
  QtOutputView* output_view;
  size_t step;
public:
  QtOutput(string filename);
  ~QtOutput();
  void update();
  void update_infos();
private slots:
  void time_change(int);
};

#endif
