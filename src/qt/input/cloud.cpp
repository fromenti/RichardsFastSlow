#include "qt/input/cloud.hpp"

QtInputCloud::QtInputCloud(QtInputData* d,Cloud* c){
  data=d;

  cloud=(c==nullptr)?data->addCloud():c;

  init_groupbox=new QGroupBox("Initial values");
  amplitude_init_widget=new QWidget;
  left_right_init_widget=new QWidget;

  final_groupbox=new QGroupBox("Final values");
  amplitude_final_widget=new QWidget;
  left_right_final_widget=new QWidget;

  init_final_widget=new QWidget;

  main_layout=new QVBoxLayout;
  init_final_layout=new QHBoxLayout;
  init_layout=new QVBoxLayout;
  final_layout=new QVBoxLayout;
  amplitude_init_layout=new QHBoxLayout;
  left_right_init_layout=new QHBoxLayout;
  amplitude_final_layout=new QHBoxLayout;
  left_right_final_layout=new QHBoxLayout;

  amplitude_init_label=new QLabel("Amplitude: ");
  amplitude_init_input=new QLineEdit();
  amplitude_init_layout->addWidget(amplitude_init_label);
  amplitude_init_layout->addWidget(amplitude_init_input);
  amplitude_init_widget->setLayout(amplitude_init_layout);

  left_init_label=new QLabel("Left: ");
  delta_left_init_label=new QLabel("  Left delta: ");
  left_init_input=new QLineEdit();
  delta_left_init_input=new QLineEdit();
  right_init_label=new QLabel("  Right: ");
  delta_right_init_label=new QLabel("  Right delta: ");
  right_init_input=new QLineEdit();
  delta_right_init_input=new QLineEdit();

  left_right_init_layout->addWidget(left_init_label);
  left_right_init_layout->addWidget(left_init_input,1);
  left_right_init_layout->addWidget(delta_left_init_label);
  left_right_init_layout->addWidget(delta_left_init_input,1);
  left_right_init_layout->addWidget(right_init_label);
  left_right_init_layout->addWidget(right_init_input,1);
  left_right_init_layout->addWidget(delta_right_init_label);
  left_right_init_layout->addWidget(delta_right_init_input,1);
  left_right_init_widget->setLayout(left_right_init_layout);

  init_layout->addWidget(amplitude_init_widget);
  init_layout->addWidget(left_right_init_widget);
  init_groupbox->setLayout(init_layout);

  amplitude_final_label=new QLabel("Amplitude: ");
  amplitude_final_input=new QLineEdit();
  amplitude_final_layout->addWidget(amplitude_final_label);
  amplitude_final_layout->addWidget(amplitude_final_input);
  amplitude_final_widget->setLayout(amplitude_final_layout);

  left_final_label=new QLabel("Left: ");
  delta_left_final_label=new QLabel("  Left delta: ");
  left_final_input=new QLineEdit();
  delta_left_final_input=new QLineEdit();
  right_final_label=new QLabel("  Right: ");
  delta_right_final_label=new QLabel("  Right delta: ");
  right_final_input=new QLineEdit();
  delta_right_final_input=new QLineEdit();

  left_right_final_layout->addWidget(left_final_label);
  left_right_final_layout->addWidget(left_final_input,1);
  left_right_final_layout->addWidget(delta_left_final_label);
  left_right_final_layout->addWidget(delta_left_final_input,1);
  left_right_final_layout->addWidget(right_final_label);
  left_right_final_layout->addWidget(right_final_input,1);
  left_right_final_layout->addWidget(delta_right_final_label);
  left_right_final_layout->addWidget(delta_right_final_input,1);
  left_right_final_widget->setLayout(left_right_final_layout);

  final_layout->addWidget(amplitude_final_widget);
  final_layout->addWidget(left_right_final_widget);
  final_groupbox->setLayout(final_layout);

  init_final_layout->addWidget(init_groupbox);
  init_final_layout->addWidget(final_groupbox);

  init_final_widget->setLayout(init_final_layout);

  remove_button=new QPushButton("Remove",this);
  main_layout->addWidget(init_final_widget);
  main_layout->addWidget(remove_button);

  double_validator=new QDoubleValidator;
  double_amplitude_validator=new QDoubleValidator;
  double_validator->setBottom(0);
  double_validator->setTop(1);

  amplitude_init_input->setValidator(double_amplitude_validator);
  left_init_input->setValidator(double_validator);
  right_init_input->setValidator(double_validator);
  delta_left_init_input->setValidator(double_validator);
  delta_right_init_input->setValidator(double_validator);

  amplitude_final_input->setValidator(double_amplitude_validator);
  left_final_input->setValidator(double_validator);
  right_final_input->setValidator(double_validator);
  delta_left_final_input->setValidator(double_validator);
  delta_right_final_input->setValidator(double_validator);

  setLayout(main_layout);
  setFrameShape(QFrame::Box);

  connect(remove_button,&QPushButton::clicked,this,&QtInputCloud::emitRemove);

  getCloud();
}


QWidget*
QtInputCloud::validate(){
  if(not amplitude_init_input->hasAcceptableInput()) return amplitude_init_input;
  if(not left_init_input->hasAcceptableInput()) return left_init_input;
  if(not right_init_input->hasAcceptableInput()) return right_init_input;
  if(not delta_left_init_input->hasAcceptableInput()) return delta_left_init_input;
  if(not delta_right_init_input->hasAcceptableInput()) return delta_right_init_input;
  if(not amplitude_final_input->hasAcceptableInput()) return amplitude_final_input;
  if(not left_final_input->hasAcceptableInput()) return left_final_input;
  if(not right_final_input->hasAcceptableInput()) return right_final_input;
  if(not delta_left_final_input->hasAcceptableInput()) return delta_left_final_input;
  if(not delta_right_final_input->hasAcceptableInput()) return delta_right_final_input;

  double li,lf,ri,rf;
  li=left_init_input->text().toDouble();
  lf=left_final_input->text().toDouble();
  ri=right_init_input->text().toDouble();
  rf=right_final_input->text().toDouble();
  if(li>=ri) return right_init_input;
  if(lf>=rf) return right_final_input;

  setCloud();

  return nullptr;
}

void
QtInputCloud::setCloud(){
  cloud->left_init=left_init_input->text().toDouble();
  cloud->right_init=right_init_input->text().toDouble();
  cloud->left_final=left_final_input->text().toDouble();
  cloud->right_final=right_final_input->text().toDouble();
  cloud->amplitude_init=amplitude_init_input->text().toDouble();
  cloud->amplitude_final=amplitude_final_input->text().toDouble();
  cloud->delta_left_init=delta_left_init_input->text().toDouble();
  cloud->delta_right_init=delta_right_init_input->text().toDouble();
  cloud->delta_left_final=delta_left_final_input->text().toDouble();
  cloud->delta_right_final=delta_right_final_input->text().toDouble();
}

void
QtInputCloud::getCloud(){
  amplitude_init_input->setText(QString::number(cloud->amplitude_init));
  left_init_input->setText(QString::number(cloud->left_init));
  right_init_input->setText(QString::number(cloud->right_init));
  delta_left_init_input->setText(QString::number(cloud->delta_left_init));
  delta_right_init_input->setText(QString::number(cloud->delta_right_init));
  amplitude_final_input->setText(QString::number(cloud->amplitude_final));
  left_final_input->setText(QString::number(cloud->left_final));
  right_final_input->setText(QString::number(cloud->right_final));
  delta_left_final_input->setText(QString::number(cloud->delta_left_final));
  delta_right_final_input->setText(QString::number(cloud->delta_right_final));
}
