#ifndef QT_INPUT_GEOMETRY_HPP
#define QT_INPUT_GEOMETRY_HPP

#include <iostream>
#include <fstream>

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QDoubleValidator>
#include <QIntValidator>
#include <QPushButton>
#include <QMessageBox>
#include "qt/input/data.hpp"


using namespace std;

class QtInputGeometry:public QWidget{
  Q_OBJECT
private:
  QtInputData* data;
  QVBoxLayout* main_layout;
  QLabel* lX_label;
  QLabel* nX_label;
  QLabel* nZ_max_label;
  QLabel* depth_label;
  QLineEdit* lX_input;
  QLineEdit* nX_input;
  QLineEdit* nZ_max_input;
  QLineEdit* depth_input;
  QPushButton* refresh_button;
  QDoubleValidator* positive_double_validator;
  QIntValidator* positive_int_validator;

public:
  QtInputGeometry(QtInputData* data);
  ~QtInputGeometry();
  QWidget* validate();
  void setGeometry();
  void getGeometry();
public slots:
  void emitGeometryChanged();
signals:
  void geometryChanged();
};

inline
QtInputGeometry::~QtInputGeometry(){
  delete positive_double_validator;
  delete positive_int_validator;
}
#endif
