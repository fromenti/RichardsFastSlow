#include "qt/input/input.hpp"

QtInput::QtInput():QWidget(){

  data=new QtInputData;

  main_layout=new QVBoxLayout;
  button_layout=new QHBoxLayout;
  tab_widget=new QTabWidget;
  button_widget=new QWidget;
  button_save=new QPushButton("Save");
  button_cancel=new QPushButton("Cancel");
  time_bar=new QScrollBar(Qt::Horizontal);

  input_physics=new QtInputPhysics;
  input_time=new QtInputTime;
  input_geometry=new QtInputGeometry(data);
  input_initial_state=new QtInputInitialState(data);
  input_pump_tab=new QtInputPumpTab(data);
  input_clouds_tab=new QtInputCloudsTab(data);
  input_view=new QtInputView(data);

  //Tab
  tab_widget->addTab(input_physics,"Physics");
  tab_widget->addTab(input_time,"Time");
  tab_widget->addTab(input_geometry,"Geometry");
  tab_widget->addTab(input_initial_state,"Initial state");
  tab_widget->addTab(input_pump_tab,"Pumps");
  tab_widget->addTab(input_clouds_tab,"Clouds");

  //Buttons
  button_layout->addWidget(button_save);
  button_layout->addWidget(button_cancel);
  button_widget->setLayout(button_layout);

  //Main
  main_layout->addWidget(tab_widget);
  main_layout->addWidget(input_view,2);
  main_layout->addWidget(time_bar);
  main_layout->addWidget(button_widget);
  setLayout(main_layout);

  //Conectors
  connect(button_save,&QPushButton::clicked,this,&QtInput::save);
  connect(button_cancel,&QPushButton::clicked,this,&QtInput::cancel);
  connect(input_geometry,&QtInputGeometry::geometryChanged,this,&QtInput::updateGeometry);
  connect(input_physics,&QtInputPhysics::physicsChanged,this,&QtInput::updateInitialState);
  connect(input_view,&QtInputView::geometryChanged,this,&QtInput::updateGeometry);
  connect(tab_widget,&QTabWidget::currentChanged,this,&QtInput::changeTabIndex);
  connect(input_initial_state,&QtInputInitialState::initialStateChanged,this,&QtInput::updateInitialState);
  connect(time_bar,&QScrollBar::valueChanged,input_view,&QtInputView::setTime);
  connect(input_pump_tab,&QtInputPumpTab::sourcesChanged,this,&QtInput::updateSource);
  connect(input_clouds_tab,&QtInputCloudsTab::sourcesChanged,this,&QtInput::updateSource);
  connect(input_view,&QtInputView::timeChanged,this,&QtInput::updateSource);
  previous_index=-1;
}

QtInput::QtInput(QString filename):QtInput(){
  load(filename.toStdString());
}

bool
QtInput::validate(){
  QWidget* widget=input_physics->validate();
  if(widget!=nullptr){
    QMessageBox msgBox;
    msgBox.setText("Incorrect physics entry");
    msgBox.exec();
    tab_widget->setCurrentWidget(input_physics);
    widget->setFocus();
    return false;
  }
  widget=input_time->validate();
  if(widget!=nullptr){
    QMessageBox msgBox;
    msgBox.setText("Incorrect time entry");
    msgBox.exec();
    tab_widget->setCurrentWidget(input_time);
    widget->setFocus();
    return false;
  }
  widget=input_geometry->validate();
  if(widget!=nullptr){
    QMessageBox msgBox;
    msgBox.setText("Incorrect geometry entry");
    msgBox.exec();
    tab_widget->setCurrentWidget(input_geometry);
    widget->setFocus();
    return false;
  }
  widget=input_initial_state->validate();
  if(widget!=nullptr){
    QMessageBox msgBox;
    msgBox.setText("Incorrect initial entry");
    msgBox.exec();
    tab_widget->setCurrentWidget(input_initial_state);
    widget->setFocus();
    return false;
  }
  return true;
}

void
QtInput::save(){
  if(validate()){
    QString filename=QFileDialog::getSaveFileName(this,"Save input","inputs/","QT input file (*.input)");
    if(not filename.isEmpty()){
      if(filename.indexOf(".input")==-1){
        filename.append(".input");
      }
      save_input(filename.toStdString());
    }
  }
}


void
QtInput::save_input(string filename){
  updateGeometry();
  fstream file;
  file.open(filename.c_str(),fstream::out|fstream::trunc|fstream::binary);
  input_physics->setPhysics();
  input_time->setTime();
  data->save(file);
  file.close();
}

void
QtInput::cancel(){
  emit exit_signal();
}

void
QtInput::load(string filename){
  fstream file;
  file.open(filename.c_str(),fstream::in|fstream::binary);
  data->load(file);
  input_physics->getPhysics();
  input_time->getTime();
  input_geometry->getGeometry();
  input_initial_state->getTanks();
  input_pump_tab->getPumps();
  input_clouds_tab->getClouds();
  file.close();
}

void
QtInput::changeTabIndex(int index){
  if(not validate()) return;
  switch(index){
    case 2:
    input_view->setStatus(QtInputView::Geom);
    break;
    case 3:
    input_view->setStatus(QtInputView::Init);
    break;
    case 4:
    input_view->setStatus(QtInputView::Sources);
    break;
    default:
    input_view->setStatus(QtInputView::Other);
    break;
  }
  switch(previous_index){
    case 2:
    updateGeometry();
    break;
    case 3:
    updateInitialState();
    break;
    default:
    break;
  }
  previous_index=index;
  input_view->update();
}

void
QtInput::updateGeometry(){
  data->updateGeometry();
  data->updateInitialState();
  input_view->update();
}

void
QtInput::updateInitialState(){
  data->updateInitialState();
  input_view->update();
}

void
QtInput::updateSource(){
  input_view->update();
}

QtInput::~QtInput(){
  delete data;
}
