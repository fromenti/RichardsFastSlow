#ifndef QT_INPUT_PHYSICS_HPP
#define QT_INPUT_PHYSICS_HPP


#include <QLabel>
#include <QWidget>
#include <QGroupBox>
#include <QComboBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QDoubleValidator>
#include <QPushButton>
#include <QMessageBox>

#include <iostream>
#include <fstream>

#include "../../kernel/physics.hpp"

using namespace std;


class QtInputPhysics:public QWidget{
  Q_OBJECT
private:
  QGroupBox* phy_common_box;
  QGroupBox* phy_model_box;
  QComboBox* phy_model_selection_box;
  QWidget* phy_base_widget;
  QLabel* g_label;
  QLabel* rho_label;
  QLabel* phi_label;
  QLabel* k0_label;
  QLabel* nivrivsat_label;
  QLabel* model_label[Physics::max_model_parameters];
  QLineEdit* g_input;
  QLineEdit* rho_input;
  QLineEdit* phi_input;
  QLineEdit* k0_input;
  QLineEdit* nivrivsat_input;
  QLineEdit* model_input[Physics::max_model_parameters];
  QHBoxLayout* phy_base_layout;
  QVBoxLayout* phy_common_layout;
  QVBoxLayout* phy_model_layout;
  QVBoxLayout* main_layout;
  QSpacerItem* phy_model_layout_spacer;
  QPushButton* refresh_button;
  QDoubleValidator* double_validator;
  QDoubleValidator* positive_double_validator;
  size_t nb_model_parameters;
public:
  QtInputPhysics();
  ~QtInputPhysics();
  QWidget* validate();
  void setPhysics();
  void getPhysics();
private slots:
  void modelChoosed(int index);
  void emitPhysicsChanged();
signals:
  void physicsChanged();
};

inline QtInputPhysics::~QtInputPhysics(){
  delete double_validator;
  delete positive_double_validator;
}

#endif
