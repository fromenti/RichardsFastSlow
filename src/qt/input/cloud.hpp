#ifndef QT_INPUT_CLOUD_HPP
#define QT_INPUT_CLOUD_HPP


#include <QFrame>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QGroupBox>
#include <QDoubleValidator>

#include "qt/input/data.hpp"

class QtInputCloud:public QFrame{
  Q_OBJECT
private:
  QtInputData* data;
  Cloud* cloud;

  QGroupBox* init_groupbox;
  QGroupBox* final_groupbox;
  QWidget* init_final_widget;

  QWidget* amplitude_init_widget;
  QWidget* left_right_init_widget;
  QWidget* amplitude_final_widget;
  QWidget* left_right_final_widget;

  QVBoxLayout* main_layout;
  QHBoxLayout* init_final_layout;
  QVBoxLayout* init_layout;
  QVBoxLayout* final_layout;

  QHBoxLayout* amplitude_init_layout;
  QHBoxLayout* left_right_init_layout;

  QHBoxLayout* amplitude_final_layout;
  QHBoxLayout* left_right_final_layout;

  QPushButton* remove_button;

  QLabel* amplitude_init_label;
  QLabel* left_init_label;
  QLabel* right_init_label;
  QLabel* delta_left_init_label;
  QLabel* delta_right_init_label;
  QLineEdit* amplitude_init_input;
  QLineEdit* left_init_input;
  QLineEdit* right_init_input;
  QLineEdit* delta_left_init_input;
  QLineEdit* delta_right_init_input;
  QLabel* amplitude_final_label;
  QLabel* left_final_label;
  QLabel* right_final_label;
  QLabel* delta_left_final_label;
  QLabel* delta_right_final_label;
  QLineEdit* amplitude_final_input;
  QLineEdit* left_final_input;
  QLineEdit* right_final_input;
  QLineEdit* delta_left_final_input;
  QLineEdit* delta_right_final_input;

  QDoubleValidator* double_validator;
  QDoubleValidator* double_amplitude_validator;
public:
  QtInputCloud(QtInputData* data,Cloud* cloud=nullptr);
  ~QtInputCloud();
  QWidget* validate();
  void setCloud();
  void getCloud();
public slots:
  void emitRemove();
signals:
  void remove(QtInputCloud* input_cloud);
};

inline
QtInputCloud::~QtInputCloud(){
  delete double_validator;
  delete double_amplitude_validator;
}

inline void
QtInputCloud::emitRemove(){
  emit remove(this);
}

#endif
