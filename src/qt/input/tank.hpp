#ifndef QT_INPUT_TANK_HPP
#define QT_INPUT_TANK_HPP

#include <QFrame>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QDoubleValidator>

#include "qt/input/data.hpp"

class QtInputTank:public QFrame{
  Q_OBJECT
private:
  QtInputData* data;
  Tank* tank;
  QWidget* saturation_widget;
  QWidget* left_right_widget;
  QWidget* bottom_top_widget;
  QVBoxLayout* main_layout;
  QHBoxLayout* saturation_layout;
  QHBoxLayout* left_right_layout;
  QHBoxLayout* bottom_top_layout;
  QPushButton* remove_button;
  QLabel* saturation_label;
  QLabel* left_label;
  QLabel* right_label;
  QLabel* top_label;
  QLabel* bottom_label;
  QLabel* delta_left_label;
  QLabel* delta_right_label;
  QLabel* delta_top_label;
  QLabel* delta_bottom_label;
  QLineEdit* saturation_input;
  QLineEdit* left_input;
  QLineEdit* right_input;
  QLineEdit* top_input;
  QLineEdit* bottom_input;
  QLineEdit* delta_left_input;
  QLineEdit* delta_right_input;
  QLineEdit* delta_top_input;
  QLineEdit* delta_bottom_input;
  QDoubleValidator* double_validator;
public:
  QtInputTank(QtInputData* data,Tank* tank=nullptr);
  ~QtInputTank();
  QWidget* validate();
  void setTank();
  void getTank();
public slots:
  void emitRemove();
signals:
  void remove(QtInputTank* input_tank);
};

inline
QtInputTank::~QtInputTank(){
  //model->removeTank(tank);
  delete double_validator;
}

inline void
QtInputTank::emitRemove(){
  emit remove(this);
}


#endif
