#ifndef QT_MAINWINDOW_HPP
#define QT_MAINWINDOW_HPP

#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QApplication>

#include "qt/input/input.hpp"


class QtMainWindow:public QMainWindow{
  Q_OBJECT
private:
  QtInput* input;
  QMenu* input_menu;
  QAction* new_act;
  QAction* load_act;
    QAction* exit_act;
public:
  QtMainWindow();
  ~QtMainWindow();
private slots:
  void new_input();
  void load_input();
  void exit();
  void exit_input();
};

inline
QtMainWindow::~QtMainWindow(){
  delete new_act;
  delete load_act;
  delete exit_act;
}
#endif
