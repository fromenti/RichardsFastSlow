#include "qt/input/physics.hpp"

QtInputPhysics::QtInputPhysics():QWidget(){
  g_label=new QLabel("Gravity (g) : ");
  rho_label=new QLabel("Fluid density (&rho;) : ");
  phi_label=new QLabel("Porosity of the soil (&phi;) : ");
  k0_label=new QLabel("Conductivity of the saturated soil (k<sub>0</sub>) : ");
  nivrivsat_label=new QLabel("Water pressure at the bottom of overland water (nivrivsat) : ");
  g_input=new QLineEdit();
  rho_input=new QLineEdit();
  phi_input=new QLineEdit();
  k0_input=new QLineEdit();
  nivrivsat_input=new QLineEdit();
  for(size_t i=0;i<Physics::max_model_parameters;++i){
    model_label[i]=new QLabel(this);
    model_input[i]=new QLineEdit(this);
  }


  //Text format
  rho_label->setTextFormat(Qt::RichText);
  phi_label->setTextFormat(Qt::RichText);
  k0_label->setTextFormat(Qt::RichText);
  for(size_t i=0;i<Physics::max_model_parameters;++i){
    model_label[i]->setTextFormat(Qt::RichText);
  }

  //Hide model parameters
  for(size_t i=0;i<Physics::max_model_parameters;++i){
    model_label[i]->hide();
    model_input[i]->hide();
  }
  //QLocal

  //Input validators
  double_validator=new QDoubleValidator;
  double_validator->setLocale(QLocale("en_US"));
  positive_double_validator=new QDoubleValidator;
  positive_double_validator->setBottom(0);

  g_input->setValidator(positive_double_validator);
  rho_input->setValidator(double_validator);
  phi_input->setValidator(double_validator);
  k0_input->setValidator(double_validator);
  nivrivsat_input->setValidator(double_validator);

  //Boxes
  phy_common_box=new QGroupBox("Common parameters");
  phy_model_box=new QGroupBox("Model parameters");
  phy_model_selection_box=new QComboBox;
  phy_model_selection_box->addItem("Brooks and Corey");

  //Button
  refresh_button=new QPushButton("Refresh");


  //Widget
  phy_base_widget=new QWidget;

  //Layouts
  main_layout=new QVBoxLayout;
  phy_base_layout=new QHBoxLayout;
  phy_common_layout=new QVBoxLayout;
  phy_model_layout=new QVBoxLayout;

  //Phy commont layout
  int vspace=20;
  phy_common_layout->addWidget(g_label);
  phy_common_layout->addWidget(g_input);
  phy_common_layout->addSpacing(vspace);
  phy_common_layout->addWidget(rho_label);
  phy_common_layout->addWidget(rho_input);
  phy_common_layout->addSpacing(vspace);
  phy_common_layout->addWidget(phi_label);
  phy_common_layout->addWidget(phi_input);
  phy_common_layout->addSpacing(vspace);
  phy_common_layout->addWidget(k0_label);
  phy_common_layout->addWidget(k0_input);
  phy_common_layout->addSpacing(vspace);
  phy_common_layout->addWidget(nivrivsat_label);
  phy_common_layout->addWidget(nivrivsat_input);
  phy_common_layout->addStretch();

  //Phy model layout
  phy_model_layout->addWidget(phy_model_selection_box);
  for(size_t i=0;i<Physics::max_model_parameters;++i){
    phy_model_layout->addSpacing(vspace);
    phy_model_layout->addWidget(model_label[i]);
    phy_model_layout->addWidget(model_input[i]);
  }
  phy_model_layout->addStretch();

  //Boxes
  phy_common_box->setLayout(phy_common_layout);
  phy_model_box->setLayout(phy_model_layout);

  //Base widget
  phy_base_layout->addWidget(phy_common_box);
  phy_base_layout->addWidget(phy_model_box);
  phy_base_widget->setLayout(phy_base_layout);

  //Main_lyaout
  main_layout->addWidget(phy_base_widget);
  main_layout->addWidget(refresh_button);
  setLayout(main_layout);

  //Conections
  connect(phy_model_selection_box,QOverload<int>::of(&QComboBox::activated),this,&QtInputPhysics::modelChoosed);

  //Display model specific interface
  nb_model_parameters=0;
  modelChoosed(phy_model_selection_box->currentIndex());

  connect(refresh_button,&QPushButton::clicked,this,&QtInputPhysics::emitPhysicsChanged);

  getPhysics();
}

void
QtInputPhysics::modelChoosed(int index){
  for(size_t i=0;i<Physics::max_model_parameters;++i){
    model_label[i]->hide();
    model_input[i]->hide();
  }
  switch(index){
  case 0:
    //Brooks and Corey
    Physics::setModel(Physics::BrooksCorey);
    nb_model_parameters=4;
    model_label[0]->setText("Minimal pressure p<sub>sat</sub> such that s(p<sub>sat</sub>)=1");
    model_input[0]->setValidator(double_validator);
    model_input[0]->setText(QString::number(Physics::model_data[0]));
    model_label[1]->setText("Minimal residual pressure (s<sub>res</sub>)");
    model_input[1]->setValidator(double_validator);
    model_input[1]->setText(QString::number(Physics::model_data[1]));
    model_label[2]->setText("&lambda; exponent");
    model_input[2]->setValidator(double_validator);
    model_input[2]->setText(QString::number(Physics::model_data[2]));
    model_label[3]->setText("&alpha; exponent");
    model_input[3]->setValidator(double_validator);
    model_input[3]->setText(QString::number(Physics::model_data[3]));
    break;
  default:
    break;
  };
  for(size_t i=0;i<nb_model_parameters;++i){
    model_label[i]->show();
    model_input[i]->show();
  }
}

QWidget*
QtInputPhysics::validate(){
  if(not g_input->hasAcceptableInput()) return g_input;
  if(not rho_input->hasAcceptableInput()) return rho_input;
  if(not phi_input->hasAcceptableInput()) return phi_input;
  if(not k0_input->hasAcceptableInput()) return k0_input;
  if(not nivrivsat_input->hasAcceptableInput()) return nivrivsat_input;
  for(size_t i=0;i<nb_model_parameters;++i){
    if(not model_input[i]->hasAcceptableInput()) return model_input[i];
  }
  return nullptr;
}

void
QtInputPhysics::getPhysics(){
  g_input->setText(QString::number(Physics::g));
  rho_input->setText(QString::number(Physics::rho));
  phi_input->setText(QString::number(Physics::phi));
  k0_input->setText(QString::number(Physics::k0));
  nivrivsat_input->setText(QString::number(Physics::nivrivsat));
  phy_model_selection_box->setCurrentIndex(Physics::model);
  for(size_t i=0;i<Physics::max_model_parameters;++i){
    model_input[i]->setText(QString::number(Physics::model_data[i]));
  }
}

void
QtInputPhysics::setPhysics(){
  Physics::g=g_input->text().toDouble();
  Physics::rho=rho_input->text().toDouble();
  Physics::phi=phi_input->text().toDouble();
  Physics::k0=k0_input->text().toDouble();
  Physics::nivrivsat=nivrivsat_input->text().toDouble();
  //Model must be already assigned by QtInputPhysics::modelChoosed
  for(size_t i=0;i<Physics::max_model_parameters;++i){
    Physics::model_data[i]=model_input[i]->text().toDouble();
  }
}

void
QtInputPhysics::emitPhysicsChanged(){
  QWidget* widget=validate();
  if(widget!=nullptr){
    QMessageBox msgBox;
    msgBox.setText("Incorrect geometry entry");
    msgBox.exec();
    widget->setFocus();
  }
  else{
    setPhysics();
    emit physicsChanged();
  }
}
