#include "qt/input/time.hpp"

QtInputTime::QtInputTime():QWidget(){
  main_layout=new QVBoxLayout;
  T_label=new QLabel("Total duration of the simulation (T)");
  nT_label=new QLabel("Number of time steps (nT)");

  T_input=new QLineEdit();
  nT_input=new QLineEdit();

  positive_double_validator=new QDoubleValidator;
  positive_double_validator->setBottom(0);
  positive_int_validator=new QIntValidator;
  positive_int_validator->setBottom(1);
  T_input->setValidator(positive_double_validator);
  nT_input->setValidator(positive_int_validator);

  int vspace=20;
  main_layout->addWidget(T_label);
  main_layout->addWidget(T_input);
  main_layout->addSpacing(vspace);
  main_layout->addWidget(nT_label);
  main_layout->addWidget(nT_input);
  main_layout->addSpacing(vspace);
  main_layout->addStretch();
  setLayout(main_layout);

  getTime();
}

QWidget*
QtInputTime::validate(){
  if(not T_input->hasAcceptableInput()) return T_input;
  if(not nT_input->hasAcceptableInput()) return nT_input;
  return nullptr;
}

void
QtInputTime::setTime(){
  Time::T=T_input->text().toDouble();
  Time::nT=nT_input->text().toULong();
}

void
QtInputTime::getTime(){
  T_input->setText(QString::number(Time::T));
  nT_input->setText(QString::number(Time::nT));
}
