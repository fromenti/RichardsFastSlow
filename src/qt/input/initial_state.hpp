#ifndef QT_INITIAL_STATE_HPP
#define QT_INITIAL_STATE_HPP

#include <iostream>
#include <fstream>

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QScrollArea>
#include <QPushButton>
#include <QPalette>
#include <QMessageBox>

#include "qt/input/tank.hpp"
#include "qt/input/data.hpp"

class QtInputInitialState:public QWidget{
  Q_OBJECT
private:
  QtInputData* data;
  QPushButton* add_button;
  QPushButton* refresh_button;
  QScrollArea* scroll_area;
  QVBoxLayout* tanks_layout;
  QWidget* tanks_widget;
  QVBoxLayout* main_layout;
  QHBoxLayout* button_layout;
  QWidget* button_widget;
public:
  QtInputInitialState(QtInputData* data);
  ~QtInputInitialState();
  QWidget* validate();
  void save(fstream& file);
  void load(fstream& file);
  void getTanks();
  void addExistingTank(Tank* tank);
public slots:
  void addTank();
  void removeTank(QtInputTank*);
  void updateInitialState();

signals:
   void initialStateChanged();
};

inline
QtInputInitialState::~QtInputInitialState(){
}

#endif
