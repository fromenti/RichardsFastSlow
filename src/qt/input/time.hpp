#ifndef QT_INPUT_TIME_HPP
#define QT_INPUT_TIME_HPP

#include <iostream>
#include <QLabel>
#include <QWidget>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QDoubleValidator>
#include <QIntValidator>
#include <fstream>

#include "../../kernel/time.hpp"

using namespace std;

class QtInputTime:public QWidget{
private:
  QLabel* T_label;
  QLabel* nT_label;
  QLineEdit* T_input;
  QLineEdit* nT_input;
  QVBoxLayout* main_layout;
  QDoubleValidator* positive_double_validator;
  QIntValidator* positive_int_validator;

public:
  QtInputTime();
  ~QtInputTime();
  QWidget* validate();
  void setTime();
  void getTime();
};

inline
QtInputTime::~QtInputTime(){
  delete positive_double_validator;
  delete positive_int_validator;
}

#endif
