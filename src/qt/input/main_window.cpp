#include "qt/input/main_window.hpp"

QtMainWindow::QtMainWindow():QMainWindow(){
  input=nullptr;
  new_act=new QAction("New input",this);
  load_act=new QAction("Load input",this);
  exit_act=new QAction("Exit",this);

  //Menu bar
  input_menu=menuBar()->addMenu("File");
  input_menu->addAction(new_act);
  input_menu->addAction(load_act);
  input_menu->addAction(exit_act);

  connect(new_act,&QAction::triggered,this,&QtMainWindow::new_input);
  connect(load_act,&QAction::triggered,this,&QtMainWindow::load_input);
  connect(exit_act,&QAction::triggered,this,&QtMainWindow::exit);
}

void
QtMainWindow::new_input(){
  input=new QtInput;
  setCentralWidget(input);
  connect(input,&QtInput::exit_signal,this,&QtMainWindow::exit_input);
}

void
QtMainWindow::load_input(){
  QString filename=QFileDialog::getOpenFileName(this,"Load input","inputs/","QT input file (*.input)");
  input=new QtInput(filename);
  setCentralWidget(input);
  connect(input,&QtInput::exit_signal,this,&QtMainWindow::exit_input);
  new_act->setEnabled(false);
  load_act->setEnabled(false);
}

void
QtMainWindow::exit_input(){
  disconnect(input,nullptr,nullptr,nullptr);
  delete input;
  input=nullptr;
  setCentralWidget(nullptr);
  new_act->setEnabled(true);
  load_act->setEnabled(true);
}

void
QtMainWindow::exit(){
  QApplication::quit();
}
