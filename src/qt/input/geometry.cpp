#include "qt/input/geometry.hpp"

QtInputGeometry::QtInputGeometry(QtInputData* m):QWidget(){
  data=m;

  main_layout=new QVBoxLayout;
  lX_label=new QLabel("Horizontal lenght of the domain (l<sub>X</sub>)");
  nX_label=new QLabel("Number of horizontal steps (n<sub>X</sub>)");
  nZ_max_label=new QLabel("Maximal number of vertical steps (n<sub>Z</sub>)");
  depth_label=new QLabel("Maximal height between h<sub>soil</sub> and h<sub>bot</sub> (depth)");

  lX_input=new QLineEdit();
  nX_input=new QLineEdit();
  nZ_max_input=new QLineEdit();
  depth_input=new QLineEdit();

//  loadGeometry();
  //Labels
  lX_label->setTextFormat(Qt::RichText);
  nX_label->setTextFormat(Qt::RichText);
  nZ_max_label->setTextFormat(Qt::RichText);
  depth_label->setTextFormat(Qt::RichText);

  //Button
  refresh_button=new QPushButton("Refresh");

  //Main layout
  int vspace=20;
  main_layout->addWidget(lX_label);
  main_layout->addWidget(lX_input);
  main_layout->addSpacing(vspace);
  main_layout->addWidget(nX_label);
  main_layout->addWidget(nX_input);
  main_layout->addSpacing(vspace);
  main_layout->addWidget(depth_label);
  main_layout->addWidget(depth_input);
  main_layout->addSpacing(vspace);
  main_layout->addWidget(nZ_max_label);
  main_layout->addWidget(nZ_max_input);
  main_layout->addSpacing(2*vspace);
  main_layout->addWidget(refresh_button);
  main_layout->addStretch();

  setLayout(main_layout);

  //Validators
  positive_double_validator=new QDoubleValidator;
  positive_double_validator->setBottom(0);
  positive_int_validator=new QIntValidator;
  positive_int_validator->setBottom(1);
  positive_int_validator->setTop(QtInputData::nmax_Qt);
  lX_input->setValidator(positive_double_validator);
  nX_input->setValidator(positive_int_validator);
  depth_input->setValidator(positive_double_validator);
  nZ_max_input->setValidator(positive_int_validator);

  connect(refresh_button,&QPushButton::clicked,this,&QtInputGeometry::emitGeometryChanged);

  getGeometry();
}

void
QtInputGeometry::emitGeometryChanged(){
  QWidget* widget=validate();
  if(widget!=nullptr){
    QMessageBox msgBox;
    msgBox.setText("Incorrect geometry entry");
    msgBox.exec();
    widget->setFocus();
  }
  else{
    setGeometry();
    emit geometryChanged();
  }
}

QWidget*
QtInputGeometry::validate(){
  if(not lX_input->hasAcceptableInput()) return lX_input;
  if(not nX_input->hasAcceptableInput()) return nX_input;
  if(not depth_input->hasAcceptableInput()) return depth_input;
  if(not nZ_max_input->hasAcceptableInput()) return nZ_max_input;
  return nullptr;
}

void
QtInputGeometry::getGeometry(){
  lX_input->setText(QString::number(data->geometry.lX));
  nX_input->setText(QString::number(data->geometry.nX));
  depth_input->setText(QString::number(data->depth));
  nZ_max_input->setText(QString::number(data->nZ_max));
}

void
QtInputGeometry::setGeometry(){
  data->geometry.lX=lX_input->text().toDouble();
  data->geometry.nX=nX_input->text().toULong();
  data->depth=depth_input->text().toDouble();
  data->nZ_max=nZ_max_input->text().toULong();
}
