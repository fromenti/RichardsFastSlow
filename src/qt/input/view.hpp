#ifndef QT_INPUT_VIEW_HPP
#define QT_INPUT_VIEW_HPP

#include <QPainter>
#include <QOpenGLWidget>
#include <QMouseEvent>
#include <GL/glut.h>

#include "math/point.hpp"
#include "math/spline.hpp"
#include "qt/input/geometry.hpp"
#include "qt/input/initial_state.hpp"
#include "qt/input/data.hpp"

using namespace std;

class QtInputView: public QOpenGLWidget{
  Q_OBJECT
public:
  enum Status{Geom,Init,Sources,Other};
private:
  Status status;
  QtInputData* data;
  static constexpr int const pointSize=6;
  static constexpr double const min_d=0.01;
  static constexpr size_t const p_none=3*QtInputData::np+QtInputData::np_ov;
  size_t selected;
  double time;
  size_t findPoint(int x,int y);
  void drawSpline(Spline& S);
  void drawOverland();
  void moveSelected(double x,double y);
public:
  QtInputView(QtInputData* data);
  ~QtInputView();
  void paintGL() override;
  void mousePressEvent(QMouseEvent* event);
  void mouseMoveEvent(QMouseEvent* event);
  void mouseReleaseEvent(QMouseEvent* event);
  void setStatus(Status status);
  void initializeGL();
  void resizeGL(int x,int h);
  void drawTriangle(size_t ix1,size_t iz1,size_t ix2,size_t iz2,size_t ix3,size_t iz3);
  void setColor(size_t ix,size_t iz);
  double getP(size_t ix,size_t iz);
  void paintTank(Tank*);
  void paintPump(Pump*);
  void paintCloud(Cloud*);

public slots:
  //void updateGeometry();
  //void updateInitialState(double factor);
  //void updateSource();
  void setTime(int v);
signals:
  void geometryChanged();
  void timeChanged();
};

inline void
QtInputView::setStatus(Status _status){
  status=_status;
}

inline double
QtInputView::getP(size_t ix,size_t iz){
  return data->initial_state->Pinit[ix][iz];
}

inline
QtInputView::~QtInputView(){
}

inline void
QtInputView::setTime(int v){
  time=double(v)/99;
  emit timeChanged();

}


#endif
