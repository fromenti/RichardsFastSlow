#ifndef QT_INPUT_DATA_HPP
#define QT_INPUT_DATA_HPP

#include "kernel/input_data.hpp"
#include "math/spline.hpp"

class QtInputData:public InputData{
private:
  size_t findSoilOver(double y,int x,int x_step);
public:
  static constexpr size_t const nmax_Qt=400;
  static constexpr size_t const np=10;
  static constexpr size_t const np_ov=5;
  double depth;
  size_t nZ_max;
  Point point[3*np+np_ov];
  Spline spline_soil;
  Spline spline_bot;
  Spline spline_sat;
  QtInputData();
  ~QtInputData();
  void initSplines();
  void initPhysics();
  void initGeometry();
  void initInitialState();
  void updateSplineBot();
  void updateSplineSat();
  void updateSplineSoil();
  void updateGeometry();
  void updateInitialState();
  void updateOverland();
  void updateOverlandAndPressure();
  double evalSplineBot(double x) const;
  double evalSplineSat(double x) const;
  double evalSplineSoil(double x) const;
  Tank* addTank();
  void removeTank(Tank* tank);
  Pump* addPump();
  void removePump(Pump* pump);
  Cloud* addCloud();
  void removeCloud(Cloud* cloud);
  void save(fstream& file);
  void load(fstream& file);
};

inline void
QtInputData::updateSplineSoil(){
  spline_soil.compute();
}

inline void
QtInputData::updateSplineBot(){
  spline_bot.compute();
}

inline void
QtInputData::updateSplineSat(){
  spline_sat.compute();
}

inline double
QtInputData::evalSplineBot(double x) const{
  return spline_bot(x);
}

inline double
QtInputData::evalSplineSat(double x) const{
  return spline_sat(x);
}

inline double
QtInputData::evalSplineSoil(double x) const{
  return spline_soil(x);
}

inline Tank*
QtInputData::addTank(){
  return initial_state->addTank();
}

inline void
QtInputData::removeTank(Tank* tank){
  initial_state->removeTank(tank);
}

inline Pump*
QtInputData::addPump(){
  return source.addPump();
}

inline void
QtInputData::removePump(Pump* pump){
  source.removePump(pump);
}

inline Cloud*
QtInputData::addCloud(){
  return source.addCloud();
}

inline void
QtInputData::removeCloud(Cloud* cloud){
  source.removeCloud(cloud);
}

#endif
