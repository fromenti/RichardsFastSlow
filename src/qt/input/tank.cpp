#include "qt/input/tank.hpp"

QtInputTank::QtInputTank(QtInputData* d,Tank* t):QFrame(){
  data=d;
  if(t==nullptr){
    tank=data->addTank();
  }
  else{
    tank=t;
  }

  saturation_widget=new QWidget;
  left_right_widget=new QWidget;
  bottom_top_widget=new QWidget;

  main_layout=new QVBoxLayout;
  saturation_layout=new QHBoxLayout;
  left_right_layout=new QHBoxLayout;
  bottom_top_layout=new QHBoxLayout;

  saturation_label=new QLabel("Saturation: ");
  saturation_input=new QLineEdit();
  saturation_layout->addWidget(saturation_label);
  saturation_layout->addWidget(saturation_input);
  saturation_widget->setLayout(saturation_layout);

  left_label=new QLabel("Left: ");
  delta_left_label=new QLabel("  Left delta: ");
  left_input=new QLineEdit();
  delta_left_input=new QLineEdit();
  right_label=new QLabel("  Right: ");
  delta_right_label=new QLabel("  Right delta: ");
  right_input=new QLineEdit();
  delta_right_input=new QLineEdit();

  left_right_layout->addWidget(left_label);
  left_right_layout->addWidget(left_input,1);
  left_right_layout->addWidget(delta_left_label);
  left_right_layout->addWidget(delta_left_input,1);
  left_right_layout->addWidget(right_label);
  left_right_layout->addWidget(right_input,1);
  left_right_layout->addWidget(delta_right_label);
  left_right_layout->addWidget(delta_right_input,1);
  left_right_widget->setLayout(left_right_layout);

  bottom_label=new QLabel("Bottom: ");
  delta_bottom_label=new QLabel("  Bottom delta: ");
  bottom_input=new QLineEdit();
  delta_bottom_input=new QLineEdit();
  top_label=new QLabel("  Top: ");
  delta_top_label=new QLabel("  Top delta: ");
  top_input=new QLineEdit();
  delta_top_input=new QLineEdit();

  bottom_top_layout->addWidget(bottom_label);
  bottom_top_layout->addWidget(bottom_input,1);
  bottom_top_layout->addWidget(delta_bottom_label);
  bottom_top_layout->addWidget(delta_bottom_input,1);
  bottom_top_layout->addWidget(top_label);
  bottom_top_layout->addWidget(top_input,1);
  bottom_top_layout->addWidget(delta_top_label);
  bottom_top_layout->addWidget(delta_top_input,1);
  bottom_top_widget->setLayout(bottom_top_layout);

  remove_button=new QPushButton("Remove",this);
  main_layout->addWidget(saturation_widget);
  main_layout->addWidget(left_right_widget);
  main_layout->addWidget(bottom_top_widget);
  main_layout->addWidget(remove_button);

  double_validator=new QDoubleValidator;
  double_validator->setBottom(0);
  double_validator->setTop(1);

  saturation_input->setValidator(double_validator);
  left_input->setValidator(double_validator);
  right_input->setValidator(double_validator);
  bottom_input->setValidator(double_validator);
  top_input->setValidator(double_validator);
  delta_left_input->setValidator(double_validator);
  delta_right_input->setValidator(double_validator);
  delta_bottom_input->setValidator(double_validator);
  delta_top_input->setValidator(double_validator);

  setLayout(main_layout);
  setFrameShape(QFrame::Box);

  connect(remove_button,&QPushButton::clicked,this,&QtInputTank::emitRemove);

  if(t==nullptr){
    tank->bottom*=data->factor;
    tank->top*=data->factor;
    tank->delta_bottom*=data->factor;
    tank->delta_top*=data->factor;
  }
  getTank();

}

QWidget*
QtInputTank::validate(){

  if(not saturation_input->hasAcceptableInput()) return saturation_input;
  if(not left_input->hasAcceptableInput()) return left_input;
  if(not right_input->hasAcceptableInput()) return right_input;
  if(not bottom_input->hasAcceptableInput()) return bottom_input;
  if(not top_input->hasAcceptableInput()) return top_input;
  if(not delta_left_input->hasAcceptableInput()) return delta_left_input;
  if(not delta_right_input->hasAcceptableInput()) return delta_right_input;
  if(not delta_bottom_input->hasAcceptableInput()) return delta_bottom_input;
  if(not delta_top_input->hasAcceptableInput()) return delta_top_input;
  double l,r,t,b;
  l=left_input->text().toDouble();
  r=right_input->text().toDouble();
  t=top_input->text().toDouble();
  b=bottom_input->text().toDouble();
  if(l>=r) return right_input;
  if(b>=t) return top_input;
  setTank();
  return nullptr;
}

void
QtInputTank::setTank(){
  double s,l,r,t,b,sl,sr,st,sb;
  s=saturation_input->text().toDouble();
  l=left_input->text().toDouble();
  r=right_input->text().toDouble();
  t=top_input->text().toDouble();
  b=bottom_input->text().toDouble();
  sl=delta_left_input->text().toDouble();
  sr=delta_right_input->text().toDouble();
  st=delta_top_input->text().toDouble();
  sb=delta_bottom_input->text().toDouble();
  tank->saturation=s;
  tank->left=l;
  tank->right=r;
  tank->bottom=b*data->factor;
  tank->top=t*data->factor;
  tank->delta_left=sl;
  tank->delta_right=sr;
  tank->delta_bottom=sb*data->factor;
  tank->delta_top=st*data->factor;
}

void
QtInputTank::getTank(){
  double factor_inv=1/data->factor;
  saturation_input->setText(QString::number(tank->saturation));
  left_input->setText(QString::number(tank->left));
  right_input->setText(QString::number(tank->right));
  top_input->setText(QString::number(tank->top*factor_inv));
  bottom_input->setText(QString::number(tank->bottom*factor_inv));
  delta_left_input->setText(QString::number(tank->delta_left));
  delta_right_input->setText(QString::number(tank->delta_right));
  delta_top_input->setText(QString::number(tank->delta_top*factor_inv));
  delta_bottom_input->setText(QString::number(tank->delta_bottom*factor_inv));
}
