#ifndef QT_INPUT_HPP
#define QT_INPUT_HPP

#include <QTabWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>
#include <QScrollBar>
#include <fstream>

#include "qt/input/physics.hpp"
#include "qt/input/time.hpp"
#include "qt/input/geometry.hpp"
#include "qt/input/initial_state.hpp"
#include "qt/input/pump_tab.hpp"
#include "qt/input/clouds_tab.hpp"
#include "qt/input/view.hpp"
#include "qt/input/data.hpp"

using namespace std;

class QtInput:public QWidget{
  Q_OBJECT
private:
  int previous_index;
  QtInputData* data;
  QVBoxLayout* main_layout;
  QHBoxLayout* button_layout;
  QTabWidget* tab_widget;
  QWidget* button_widget;
  QPushButton* button_save;
  QPushButton* button_cancel;
  QScrollBar* time_bar;
  QtInputPhysics* input_physics;
  QtInputTime* input_time;
  QtInputGeometry* input_geometry;
  QtInputInitialState* input_initial_state;
  QtInputPumpTab* input_pump_tab;
  QtInputCloudsTab* input_clouds_tab;
  QtInputView* input_view;
  void initDefaultGeometry();
  bool validate();
  void load(string filename);
public:
  QtInput();
  QtInput(QString filename);
  ~QtInput();
  void save_input(string filename);
  const QtInputData* getData() const;
signals:
  void run_signal();
  void exit_signal();
private slots:
  void save();
  void cancel();
  void changeTabIndex(int index);
  void updateGeometry();
  void updateInitialState();
  void updateSource();
};


inline const QtInputData*
QtInput::getData() const{
  return data;
}
#endif
