
#ifndef QT_INPUT_PUMP_TAB_HPP
#define QT_INPUT_PUMP_TAB_HPP

#include <iostream>
#include <fstream>

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QScrollArea>
#include <QPushButton>
#include <QPalette>
#include <QMessageBox>

#include "qt/input/pump.hpp"

#include "qt/input/data.hpp"

class QtInputPumpTab:public QWidget{
  Q_OBJECT
private:
  QtInputData* data;
  QPushButton* add_button;
  QPushButton* refresh_button;
  QScrollArea* scroll_area;
  QVBoxLayout* pumps_layout;
  QWidget* pumps_widget;
  QVBoxLayout* main_layout;
  QHBoxLayout* button_layout;
  QWidget* button_widget;
public:
  QtInputPumpTab(QtInputData* data);
  ~QtInputPumpTab();
  QWidget* validate();
  void addExistingPump(Pump* pump);
  void getPumps();
public slots:
   void addPump();
   void removePump(QtInputPump*);
   void updateSources();
signals:
   void sourcesChanged();
};

inline
QtInputPumpTab::~QtInputPumpTab(){
}

#endif
