#ifndef QT_INPUT_PUMP_HPP
#define QT_INPUT_PUMP_HPP


#include <QFrame>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QGroupBox>
#include <QDoubleValidator>
#include "qt/input/data.hpp"

class QtInputPump:public QFrame{
  Q_OBJECT
private:
  QtInputData* data;
  Pump* pump;
  QGroupBox* init_groupbox;
  QGroupBox* final_groupbox;
  QWidget* init_final_widget;

  QWidget* amplitude_init_widget;
  QWidget* left_right_init_widget;
  QWidget* bottom_top_init_widget;
  QWidget* amplitude_final_widget;
  QWidget* left_right_final_widget;
  QWidget* bottom_top_final_widget;

  QVBoxLayout* main_layout;
  QHBoxLayout* init_final_layout;
  QVBoxLayout* init_layout;
  QVBoxLayout* final_layout;

  QHBoxLayout* amplitude_init_layout;
  QHBoxLayout* left_right_init_layout;
  QHBoxLayout* bottom_top_init_layout;

  QHBoxLayout* amplitude_final_layout;
  QHBoxLayout* left_right_final_layout;
  QHBoxLayout* bottom_top_final_layout;

  QPushButton* remove_button;

  QLabel* amplitude_init_label;
  QLabel* left_init_label;
  QLabel* right_init_label;
  QLabel* top_init_label;
  QLabel* bottom_init_label;
  QLabel* delta_left_init_label;
  QLabel* delta_right_init_label;
  QLabel* delta_top_init_label;
  QLabel* delta_bottom_init_label;
  QLineEdit* amplitude_init_input;
  QLineEdit* left_init_input;
  QLineEdit* right_init_input;
  QLineEdit* top_init_input;
  QLineEdit* bottom_init_input;
  QLineEdit* delta_left_init_input;
  QLineEdit* delta_right_init_input;
  QLineEdit* delta_top_init_input;
  QLineEdit* delta_bottom_init_input;
  QLabel* amplitude_final_label;
  QLabel* left_final_label;
  QLabel* right_final_label;
  QLabel* top_final_label;
  QLabel* bottom_final_label;
  QLabel* delta_left_final_label;
  QLabel* delta_right_final_label;
  QLabel* delta_top_final_label;
  QLabel* delta_bottom_final_label;
  QLineEdit* amplitude_final_input;
  QLineEdit* left_final_input;
  QLineEdit* right_final_input;
  QLineEdit* top_final_input;
  QLineEdit* bottom_final_input;
  QLineEdit* delta_left_final_input;
  QLineEdit* delta_right_final_input;
  QLineEdit* delta_top_final_input;
  QLineEdit* delta_bottom_final_input;

  QDoubleValidator* double_validator;
  QDoubleValidator* double_amplitude_validator;
public:
  QtInputPump(QtInputData* data,Pump* pump=nullptr);
  ~QtInputPump();
  QWidget* validate();
  void setPump();
  void getPump();
public slots:
  void emitRemove();
signals:
  void remove(QtInputPump* input_pump);
};

inline
QtInputPump::~QtInputPump(){
  delete double_validator;
  delete double_amplitude_validator;
}

inline void
QtInputPump::emitRemove(){
  emit remove(this);
}

#endif
