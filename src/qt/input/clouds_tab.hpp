#ifndef QT_INPUT_CLOUDS_TAB_HPP
#define QT_INPUT_CLOUDS_TAB_HPP

#include <iostream>
#include <fstream>

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QScrollArea>
#include <QPushButton>
#include <QPalette>
#include <QMessageBox>

#include "qt/input/cloud.hpp"
#include "kernel/input_data.hpp"

class QtInputCloudsTab:public QWidget{
  Q_OBJECT
private:
  QtInputData* data;
  QPushButton* add_button;
  QPushButton* refresh_button;
  QScrollArea* scroll_area;
  QVBoxLayout* clouds_layout;
  QWidget* clouds_widget;
  QVBoxLayout* main_layout;
  QHBoxLayout* button_layout;
  QWidget* button_widget;
public:
  QtInputCloudsTab(QtInputData* data);
  ~QtInputCloudsTab();
  QWidget* validate();
  void getClouds();
  void addExistingCloud(Cloud* cloud);
public slots:
   void addCloud();
   void removeCloud(QtInputCloud*);
   void updateSources();
signals:
   void sourcesChanged();
};

inline
QtInputCloudsTab::~QtInputCloudsTab(){
}

#endif
