#include "qt/input/pump.hpp"

QtInputPump::QtInputPump(QtInputData* d,Pump* p){
  data=d;

  pump=(p==nullptr)?data->addPump():p;

  init_groupbox=new QGroupBox("Initial values");
  amplitude_init_widget=new QWidget;
  left_right_init_widget=new QWidget;
  bottom_top_init_widget=new QWidget;

  final_groupbox=new QGroupBox("Final values");
  amplitude_final_widget=new QWidget;
  left_right_final_widget=new QWidget;
  bottom_top_final_widget=new QWidget;

  init_final_widget=new QWidget;

  main_layout=new QVBoxLayout;
  init_final_layout=new QHBoxLayout;
  init_layout=new QVBoxLayout;
  final_layout=new QVBoxLayout;
  amplitude_init_layout=new QHBoxLayout;
  left_right_init_layout=new QHBoxLayout;
  bottom_top_init_layout=new QHBoxLayout;
  amplitude_final_layout=new QHBoxLayout;
  left_right_final_layout=new QHBoxLayout;
  bottom_top_final_layout=new QHBoxLayout;


  amplitude_init_label=new QLabel("Amplitude: ");
  amplitude_init_input=new QLineEdit();
  amplitude_init_layout->addWidget(amplitude_init_label);
  amplitude_init_layout->addWidget(amplitude_init_input);
  amplitude_init_widget->setLayout(amplitude_init_layout);

  left_init_label=new QLabel("Left: ");
  delta_left_init_label=new QLabel("  Left delta: ");
  left_init_input=new QLineEdit();
  delta_left_init_input=new QLineEdit();
  right_init_label=new QLabel("  Right: ");
  delta_right_init_label=new QLabel("  Right delta: ");
  right_init_input=new QLineEdit();
  delta_right_init_input=new QLineEdit();

  left_right_init_layout->addWidget(left_init_label);
  left_right_init_layout->addWidget(left_init_input,1);
  left_right_init_layout->addWidget(delta_left_init_label);
  left_right_init_layout->addWidget(delta_left_init_input,1);
  left_right_init_layout->addWidget(right_init_label);
  left_right_init_layout->addWidget(right_init_input,1);
  left_right_init_layout->addWidget(delta_right_init_label);
  left_right_init_layout->addWidget(delta_right_init_input,1);
  left_right_init_widget->setLayout(left_right_init_layout);

  bottom_init_label=new QLabel("Bottom: ");
  delta_bottom_init_label=new QLabel("  Bottom delta: ");
  bottom_init_input=new QLineEdit();
  delta_bottom_init_input=new QLineEdit();
  top_init_label=new QLabel("  Top: ");
  delta_top_init_label=new QLabel("  Top delta: ");
  top_init_input=new QLineEdit();
  delta_top_init_input=new QLineEdit();

  bottom_top_init_layout->addWidget(bottom_init_label);
  bottom_top_init_layout->addWidget(bottom_init_input,1);
  bottom_top_init_layout->addWidget(delta_bottom_init_label);
  bottom_top_init_layout->addWidget(delta_bottom_init_input,1);
  bottom_top_init_layout->addWidget(top_init_label);
  bottom_top_init_layout->addWidget(top_init_input,1);
  bottom_top_init_layout->addWidget(delta_top_init_label);
  bottom_top_init_layout->addWidget(delta_top_init_input,1);
  bottom_top_init_widget->setLayout(bottom_top_init_layout);

  init_layout->addWidget(amplitude_init_widget);
  init_layout->addWidget(left_right_init_widget);
  init_layout->addWidget(bottom_top_init_widget);
  init_groupbox->setLayout(init_layout);


  amplitude_final_label=new QLabel("Amplitude: ");
  amplitude_final_input=new QLineEdit();
  amplitude_final_layout->addWidget(amplitude_final_label);
  amplitude_final_layout->addWidget(amplitude_final_input);
  amplitude_final_widget->setLayout(amplitude_final_layout);

  left_final_label=new QLabel("Left: ");
  delta_left_final_label=new QLabel("  Left delta: ");
  left_final_input=new QLineEdit();
  delta_left_final_input=new QLineEdit();
  right_final_label=new QLabel("  Right: ");
  delta_right_final_label=new QLabel("  Right delta: ");
  right_final_input=new QLineEdit();
  delta_right_final_input=new QLineEdit();

  left_right_final_layout->addWidget(left_final_label);
  left_right_final_layout->addWidget(left_final_input,1);
  left_right_final_layout->addWidget(delta_left_final_label);
  left_right_final_layout->addWidget(delta_left_final_input,1);
  left_right_final_layout->addWidget(right_final_label);
  left_right_final_layout->addWidget(right_final_input,1);
  left_right_final_layout->addWidget(delta_right_final_label);
  left_right_final_layout->addWidget(delta_right_final_input,1);
  left_right_final_widget->setLayout(left_right_final_layout);

  bottom_final_label=new QLabel("Bottom: ");
  delta_bottom_final_label=new QLabel("  Bottom delta: ");
  bottom_final_input=new QLineEdit();
  delta_bottom_final_input=new QLineEdit();
  top_final_label=new QLabel("  Top: ");
  delta_top_final_label=new QLabel("  Top delta: ");
  top_final_input=new QLineEdit();
  delta_top_final_input=new QLineEdit();

  bottom_top_final_layout->addWidget(bottom_final_label);
  bottom_top_final_layout->addWidget(bottom_final_input,1);
  bottom_top_final_layout->addWidget(delta_bottom_final_label);
  bottom_top_final_layout->addWidget(delta_bottom_final_input,1);
  bottom_top_final_layout->addWidget(top_final_label);
  bottom_top_final_layout->addWidget(top_final_input,1);
  bottom_top_final_layout->addWidget(delta_top_final_label);
  bottom_top_final_layout->addWidget(delta_top_final_input,1);
  bottom_top_final_widget->setLayout(bottom_top_final_layout);

  final_layout->addWidget(amplitude_final_widget);
  final_layout->addWidget(left_right_final_widget);
  final_layout->addWidget(bottom_top_final_widget);
  final_groupbox->setLayout(final_layout);

  init_final_layout->addWidget(init_groupbox);
  init_final_layout->addWidget(final_groupbox);

  init_final_widget->setLayout(init_final_layout);


  remove_button=new QPushButton("Remove",this);
  main_layout->addWidget(init_final_widget);
  main_layout->addWidget(remove_button);

  double_validator=new QDoubleValidator;
  double_amplitude_validator=new QDoubleValidator;
  double_validator->setBottom(0);
  double_validator->setTop(1);

  amplitude_init_input->setValidator(double_amplitude_validator);
  left_init_input->setValidator(double_validator);
  right_init_input->setValidator(double_validator);
  bottom_init_input->setValidator(double_validator);
  top_init_input->setValidator(double_validator);
  delta_left_init_input->setValidator(double_validator);
  delta_right_init_input->setValidator(double_validator);
  delta_bottom_init_input->setValidator(double_validator);
  delta_top_init_input->setValidator(double_validator);

  amplitude_final_input->setValidator(double_amplitude_validator);
  left_final_input->setValidator(double_validator);
  right_final_input->setValidator(double_validator);
  bottom_final_input->setValidator(double_validator);
  top_final_input->setValidator(double_validator);
  delta_left_final_input->setValidator(double_validator);
  delta_right_final_input->setValidator(double_validator);
  delta_bottom_final_input->setValidator(double_validator);
  delta_top_final_input->setValidator(double_validator);

  setLayout(main_layout);
  setFrameShape(QFrame::Box);

  connect(remove_button,&QPushButton::clicked,this,&QtInputPump::emitRemove);

  if(p==nullptr){
    pump->bottom_init*=data->factor;
    pump->top_init*=data->factor;
    pump->delta_bottom_init*=data->factor;
    pump->delta_top_init*=data->factor;
    pump->bottom_final*=data->factor;
    pump->top_final*=data->factor;
    pump->delta_bottom_final*=data->factor;
    pump->delta_top_final*=data->factor;
  }
  getPump();
}


QWidget*
QtInputPump::validate(){
  if(not amplitude_init_input->hasAcceptableInput()) return amplitude_init_input;
  if(not left_init_input->hasAcceptableInput()) return left_init_input;
  if(not right_init_input->hasAcceptableInput()) return right_init_input;
  if(not bottom_init_input->hasAcceptableInput()) return bottom_init_input;
  if(not top_init_input->hasAcceptableInput()) return top_init_input;
  if(not delta_left_init_input->hasAcceptableInput()) return delta_left_init_input;
  if(not delta_right_init_input->hasAcceptableInput()) return delta_right_init_input;
  if(not delta_bottom_init_input->hasAcceptableInput()) return delta_bottom_init_input;
  if(not delta_top_init_input->hasAcceptableInput()) return delta_top_init_input;
  if(not amplitude_final_input->hasAcceptableInput()) return amplitude_final_input;
  if(not left_final_input->hasAcceptableInput()) return left_final_input;
  if(not right_final_input->hasAcceptableInput()) return right_final_input;
  if(not bottom_final_input->hasAcceptableInput()) return bottom_final_input;
  if(not top_final_input->hasAcceptableInput()) return top_final_input;
  if(not delta_left_final_input->hasAcceptableInput()) return delta_left_final_input;
  if(not delta_right_final_input->hasAcceptableInput()) return delta_right_final_input;
  if(not delta_bottom_final_input->hasAcceptableInput()) return delta_bottom_final_input;
  if(not delta_top_final_input->hasAcceptableInput()) return delta_top_final_input;

  double li,lf,ri,rf,ti,tf,bi,bf;
  li=left_init_input->text().toDouble();
  lf=left_final_input->text().toDouble();
  ri=right_init_input->text().toDouble();
  rf=right_final_input->text().toDouble();
  ti=top_init_input->text().toDouble();
  tf=top_final_input->text().toDouble();
  bi=bottom_init_input->text().toDouble();
  bf=bottom_final_input->text().toDouble();
  if(li>=ri) return right_init_input;
  if(bi>=ti) return top_init_input;
  if(lf>=rf) return right_final_input;
  if(bf>=tf) return top_final_input;
  setPump();
  return nullptr;
}

void
QtInputPump::setPump(){
  double li,lf,ri,rf,ti,tf,bi,bf;
  li=left_init_input->text().toDouble();
  lf=left_final_input->text().toDouble();
  ri=right_init_input->text().toDouble();
  rf=right_final_input->text().toDouble();
  ti=top_init_input->text().toDouble();
  tf=top_final_input->text().toDouble();
  bi=bottom_init_input->text().toDouble();
  bf=bottom_final_input->text().toDouble();
  pump->left_init=li;
  pump->right_init=ri;
  pump->top_init=ti*data->factor;
  pump->bottom_init=bi*data->factor;
  pump->left_final=lf;
  pump->right_final=rf;
  pump->top_final=tf*data->factor;
  pump->bottom_final=bf*data->factor;
  pump->amplitude_init=amplitude_init_input->text().toDouble();
  pump->amplitude_final=amplitude_final_input->text().toDouble();
  pump->delta_left_init=delta_left_init_input->text().toDouble();
  pump->delta_right_init=delta_right_init_input->text().toDouble();
  pump->delta_top_init=delta_top_init_input->text().toDouble()*data->factor;
  pump->delta_bottom_init=delta_bottom_init_input->text().toDouble()*data->factor;
  pump->delta_left_final=delta_left_final_input->text().toDouble();
  pump->delta_right_final=delta_right_final_input->text().toDouble();
  pump->delta_top_final=delta_top_final_input->text().toDouble()*data->factor;
  pump->delta_bottom_final=delta_bottom_final_input->text().toDouble()*data->factor;
}

void
QtInputPump::getPump(){
  double factor_inv=1/data->factor;
  amplitude_init_input->setText(QString::number(pump->amplitude_init));
  left_init_input->setText(QString::number(pump->left_init));
  right_init_input->setText(QString::number(pump->right_init));
  top_init_input->setText(QString::number(pump->top_init*factor_inv));
  bottom_init_input->setText(QString::number(pump->bottom_init*factor_inv));
  delta_left_init_input->setText(QString::number(pump->delta_left_init));
  delta_right_init_input->setText(QString::number(pump->delta_right_init));
  delta_top_init_input->setText(QString::number(pump->delta_top_init*factor_inv));
  delta_bottom_init_input->setText(QString::number(pump->delta_bottom_init*factor_inv));
  amplitude_final_input->setText(QString::number(pump->amplitude_final));
  left_final_input->setText(QString::number(pump->left_final));
  right_final_input->setText(QString::number(pump->right_final));
  top_final_input->setText(QString::number(pump->top_final*factor_inv));
  bottom_final_input->setText(QString::number(pump->bottom_final*factor_inv));
  delta_left_final_input->setText(QString::number(pump->delta_left_final));
  delta_right_final_input->setText(QString::number(pump->delta_right_final));
  delta_top_final_input->setText(QString::number(pump->delta_top_final*factor_inv));
  delta_bottom_final_input->setText(QString::number(pump->delta_bottom_final*factor_inv));
}
