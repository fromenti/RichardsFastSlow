#ifndef DEBUG_HPP
#define DEBUG_HPP

//#define MYDEBUG

#include <iostream>

using namespace std;

class Debug{
public:
  static const int level=0;
  static const size_t ix=44;
  static void display(string name,double* u,size_t start,size_t end);
  static void pause();
  static bool debug_Thomas;
};




inline void Debug::display(string name,double* u,size_t start,size_t end){
  cout<<"===== "<<name<<" ====="<<endl;
  for(size_t i=start;i<end;++i){
    cout<<i<<" : "<<u[i]<<endl;
  }
  cout<<"============================"<<endl;
}

inline void Debug::pause(){
  char a;
  cin>>a;
}
#endif
