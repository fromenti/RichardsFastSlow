#include <iostream>
#include <QApplication>
#include "qt/input/main_window.hpp"

using namespace std;

int main(int argc,char** argv){
  QApplication app(argc,argv);
  QtMainWindow* window;
  window=new QtMainWindow;
  window->resize(1280,1024);
  window->show();
  return app.exec();

}
