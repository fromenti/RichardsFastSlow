QT_FLAGS	=-fPIC `pkg-config --cflags --libs Qt6OpenGLWidgets`
QT_LIB		=`pkg-config --libs Qt6OpenGLWidgets` -lGL -lGLU
QT_MOC		= /usr/lib/qt6/moc
GPP		= g++

FLAGS 		= -g -O3 -Isrc -Wfatal-errors

QT_INPUT_FILES	= main_window data input physics time geometry initial_state view tank pump pump_tab cloud clouds_tab
QT_INPUT_MOC_FILES	= main_window physics input view geometry initial_state tank pump_tab pump cloud clouds_tab
MATH_INPUT_FILES	= poly3 algo spline
KERNEL_INPUT_FILES = physics time geometry initial_state source input_data basin

QT_OUTPUT_FILES	= main_window solver output_view output
QT_OUTPUT_MOC_FILES	= main_window solver output
MATH_OUTPUT_FILES	= algo
KERNEL_OUTPUT_FILES = physics time geometry initial_state source input_data solution solver piccard horizontal_problem overland all_vertical_richards richards_evolutive_time richards basin


QT_INPUT_OBJS	= $(addprefix obj/qt/input/,$(addsuffix .o,$(QT_INPUT_FILES)))
QT_INPUT_MOCS	= $(addprefix moc/input/,$(addsuffix .cpp,$(QT_INPUT_MOC_FILES)))
MATH_INPUT_OBJS	= $(addprefix obj/math/,$(addsuffix .o,$(MATH_INPUT_FILES)))
KERNEL_INPUT_OBJS = $(addprefix obj/kernel/,$(addsuffix .o,$(KERNEL_INPUT_FILES)))

QT_OUTPUT_OBJS	= $(addprefix obj/qt/output/,$(addsuffix .o,$(QT_OUTPUT_FILES)))
QT_OUTPUT_MOCS	= $(addprefix moc/output/,$(addsuffix .cpp,$(QT_OUTPUT_MOC_FILES)))
MATH_OUTPUT_OBJS	= $(addprefix obj/math/,$(addsuffix .o,$(MATH_OUTPUT_FILES)))
KERNEL_OUTPUT_OBJS = $(addprefix obj/kernel/,$(addsuffix .o,$(KERNEL_OUTPUT_FILES)))

MATH_SOLVER_OBJS	= $(addprefix obj/math/,$(addsuffix .o,$(MATH_OUTPUT_FILES)))
KERNEL_SOLVER_OBJS = $(addprefix obj/kernel/,$(addsuffix .o,$(KERNEL_OUTPUT_FILES)))

EXE_INPUT = RichardsFastSlowInput
EXE_OUTPUT = RichardsFastSlowOutput
EXE_SOLVER = RichardsFastSlowSolver

all: $(EXE_OUTPUT) $(EXE_INPUT)
input: $(EXE_INPUT)
output: $(EXE_OUTPUT)
solver: $(EXE_SOLVER)

$(EXE_INPUT) : src/main_input.cpp $(KERNEL_INPUT_OBJS) $(MATH_INPUT_OBJS) $(QT_INPUT_MOCS) $(QT_INPUT_OBJS)
			$(GPP) $(FLAGS) $(QT_FLAGS) $^ $(QT_LIB) -o $@

$(EXE_OUTPUT) : src/main_output.cpp src/debug.cpp $(KERNEL_OUTPUT_OBJS) $(MATH_OUTPUT_OBJS) $(QT_OUTPUT_MOCS) $(QT_OUTPUT_OBJS)
			$(GPP) $(FLAGS) $(QT_FLAGS) $^ $(QT_LIB) -o $@

$(EXE_SOLVER) : src/main_solver.cpp $(KERNEL_SOLVER_OBJS) $(MATH_SOLVER_OBJS)
			$(GPP) $(FLAGS) $^ -o $@

obj/qt/%.o : src/qt/%.cpp src/qt/%.hpp
	$(GPP) $(FLAGS) $(QT_FLAGS) -c $< -o $@

obj/math/%.o : src/math/%.cpp src/math/%.hpp
	$(GPP) $(FLAGS) -c $< -o $@

obj/%.o : src/%.cpp src/%.hpp
	$(GPP) $(FLAGS) -c $< -o $@

moc/%.cpp : src/qt/%.hpp
	$(QT_MOC) $< -o $@

clean:
	-$(RM) -r $(EXE) $(EXE_OUTPUT) moc/input/*.cpp moc/output/*.cpp moc/*.cpp obj/*.o obj/kernel/*.o obj/math/*.o obj/qt/input/*.o obj/qt/output/*.o obj/qt/*.o src/*~ src/qt/*~ src/math/*~ doc/*
